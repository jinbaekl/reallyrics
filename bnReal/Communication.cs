﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Threading;
using System.Net.Sockets;

namespace bnReal
{
    internal class GZipWebResponse : WebResponse
    {
        WebResponse response;
        internal GZipWebResponse(WebResponse resp)
        {
            response = resp;
        }
        public override System.IO.Stream GetResponseStream()
        {
            return new Ionic.Zlib.GZipStream(response.GetResponseStream(), Ionic.Zlib.CompressionMode.Decompress); //Uncompress
        }
    }

    public static class Communication
    {
        private static string accpt = "*/*";
        private static CookieContainer ccMain = new CookieContainer();
        private static string useragent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36";
        private static ICredentials ncMain = new NetworkCredential();

        private static string[] serverurl = new string[] {
            "http://lyrics.alsong.co.kr", "http://lyrics.alsong.jp" };
        public static int aserver = 0;

        public static HttpWebRequest Communicate(string target, Action<int, string> after, string speReferer = "", string speUA = "")
        {
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                if (speUA.Length > 0)
                {
                    hwr.UserAgent = speUA;
                }
                else
                {
                    hwr.UserAgent = useragent;
                }
                hwr.Credentials = ncMain;
                if (speReferer.Length > 0)
                {
                    hwr.Headers[HttpRequestHeader.Referer] = speReferer;
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.Referer] = "http://"+new Uri(target).Host;
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");

                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress

                            
                            using (var stream = resp.GetResponseStream())
                            {
                                MemoryStream ms = new MemoryStream();
                                stream.CopyTo(ms);
                                ms.Position = 0;
                                using (StreamReader sr = new StreamReader(ms))
                                {
                                    string got = sr.ReadToEnd();
                                    after(0, got);
                                }
                                ms.Dispose();
                            }
                        }
                        else
                        {
                            after(-1, "");
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, ex.Status.ToString());
                    }
                    catch (Exception ex)
                    {
                        after(-2, ex.GetType().ToString());
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, ex.Status.ToString());
            }
            catch (Exception ex)
            {
                after(-2, ex.GetType().ToString());
            }
            return null;
        }

        public static HttpWebRequest Communicate(string target, Action<int, Stream> after, string speReferer = "", string speUA = "")
        {
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                if (speUA.Length > 0)
                {
                    hwr.UserAgent = speUA;
                }
                else
                {
                    hwr.UserAgent = useragent;
                }
                hwr.Credentials = ncMain;
                if (speReferer.Length > 0)
                {
                    hwr.Headers[HttpRequestHeader.Referer] = speReferer;
                }
                else
                {
                    hwr.Headers[HttpRequestHeader.Referer] = "http://" + new Uri(target).Host;
                }
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");

                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            if (resp.Headers[HttpRequestHeader.ContentEncoding] != null && resp.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip"))
                                resp = new GZipWebResponse(resp); //If gzipped response, uncompress


                            var stream = resp.GetResponseStream();
                            MemoryStream ms = new MemoryStream();
                            stream.CopyTo(ms);
                            ms.Position = 0;
                            after(0, ms);
                        }
                        else
                        {
                            after(-1, null);
                        }
                    }
                    catch (WebException ex)
                    {
                        after(-1, null);
                    }
                    catch (Exception ex)
                    {
                        after(-2, null);
                    }
                }, null);
                return hwr;
            }
            catch (WebException ex)
            {
                after(-1, null);
            }
            catch (Exception ex)
            {
                after(-2, null);
            }
            return null;
        }

        public static HttpWebRequest CommunicateBinary(string target, Action<int, Stream, string> after)
        {
            try
            {
                HttpWebRequest hwr = WebRequest.CreateHttp(target);
                //hwr.AllowAutoRedirect
                hwr.Method = "GET";
                hwr.Accept = accpt;
                hwr.CookieContainer = ccMain;
                hwr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                hwr.Headers[HttpRequestHeader.CacheControl] = "max-age=0";
                hwr.UserAgent = useragent;
                hwr.Headers[HttpRequestHeader.Referer] = "http://"+new Uri(target).Host+"/";
                //hwr.Headers[HttpRequestHeader.Referer] = null;
                hwr.Headers[HttpRequestHeader.AcceptLanguage] = CultureInfo.CurrentCulture.Name;
                //hwr.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString("r");
                hwr.BeginGetResponse((a) =>
                {
                    try
                    {
                        if (a == null || hwr == null)
                        {
                            return;
                        }
                        if (a.IsCompleted)
                        {
                            var resp = hwr.EndGetResponse(a);
                            using (var stream = resp.GetResponseStream())
                            {
                                MemoryStream ms = new MemoryStream();
                                stream.CopyTo(ms);
                                ms.Position = 0;
                                after(0, ms, resp.ResponseUri.OriginalString);
                            }
                        }
                    }
                    catch (WebException)
                    {
                        after(-1, null, string.Empty);
                    }
                    catch (Exception)
                    {
                        after(-2, null, string.Empty);
                    }
                }, null);
                return hwr;
            }
            catch (WebException)
            {
                after(-1, null, string.Empty);
            }
            catch (Exception)
            {
                after(-2, null, string.Empty);
            }
            return null;
        }

        public static HttpWebRequest CommunicateALSong(string reqstr, Action<int, Stream> after)
        {
            return Communicate(serverurl[aserver] + reqstr, after, serverurl[aserver], "ALSong/1.3 CFNetwork/548.0.3 Darwin/11.0.0");
        }
    }
}
