﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Compression;
using Windows.Storage.FileProperties;
using Windows.Storage.Search;
using Windows.Storage.Streams;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows;

namespace bnReal
{
    public static class LocalLyrics
    {
        public static Dictionary<string, string> Keys = new Dictionary<string, string>();
        public static Dictionary<string, List<string>> SimpleKeys = new Dictionary<string, List<string>>();
        private static Dictionary<string, string> LoadingKeys = new Dictionary<string, string>();
        public static List<string> LRCFound = new List<string>();
        private static List<string> LoadingLRC = new List<string>();

        public static bool Initialized = false;
        public static MediaLibrary MLink = null;
        public static Action Doneinit = null;
        public static Action<double,string> ProgressReport = null;

        public static bool Working = false;

        public async static void Initialize(MediaLibrary ml, bool checkonly = false, bool force = false)
        {
            MLink = ml;

            await LoadMatchIndex();
            await LoadLRCTable();

            if (!checkonly && (force || Keys.Count == 0) && ml.Songs.Count > 0)
            {
                BuildMusicTable();
            }
            else
            {
                Initialized = true;
            }
        }

        private static async Task LoadLRCTable()
        {
            StorageFile file = null;
            try
            {
                file = await ApplicationData.Current.LocalCacheFolder.
                               GetFileAsync("lrctable.tmp");
            }
            catch
            {

            }
            if (file != null)
            {
                using (IInputStream inStream = await file.OpenSequentialReadAsync())
                {
                    var decompressor = new Decompressor(inStream);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        var bytes = await RandomAccessStream.CopyAsync(decompressor, ms.AsOutputStream());
                        ms.Seek(0, SeekOrigin.Begin);
                        DataContractSerializer serializer =
                                new DataContractSerializer(typeof(List<string>));
                        LRCFound = (List<string>)serializer.ReadObject(ms);
                    }
                    Debug.WriteLine(LRCFound.Count + "개의 가사");
                }
            }
        }

        private static async Task LoadMatchIndex()
        {
            StorageFile file = null;
            try
            {
                file = await ApplicationData.Current.LocalCacheFolder.
                               GetFileAsync("lrcmatch.tmp");
            }
            catch
            {

            }
            if (file != null)
            {
                using (IInputStream inStream = await file.OpenSequentialReadAsync())
                {
                    var decompressor = new Decompressor(inStream);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        var bytes = await RandomAccessStream.CopyAsync(decompressor, ms.AsOutputStream());
                        ms.Seek(0, SeekOrigin.Begin);
                        DataContractSerializer serializer =
                                new DataContractSerializer(typeof(Dictionary<string, string>));
                        Keys = (Dictionary<string, string>)serializer.ReadObject(ms);
                    }
                    Debug.WriteLine(Keys.Count + " 항목 인덱싱 파일 찾음");
                }
            }
        }

        public static void BuildMusicTable()
        {
            if(Working)
            {
                if(ProgressReport != null)
                {
                    ProgressReport(0, string.Empty);
                }
                return;
            }
            Working = true;
            if (MLink == null || MLink.Songs.Count == 0)
            {
                return;
            }

            LoadingKeys.Clear();
            LoadingLRC.Clear();
            itree = 0;
            maxtree = 0;
            foreach (var song in MLink.Songs)
            {
                string key = LibraryKeyword(song);
                if (!LoadingKeys.ContainsKey(key))
                {
                    LoadingKeys.Add(key, null);
                    string simple = string.Format("{0}|{1}", song.TrackNumber, CleanExt(song.Name.Trim()));
                    if(SimpleKeys.ContainsKey(simple))
                    {
                        SimpleKeys[simple].Add(key);
                    }
                    else
                    {
                        var lstk = new List<string>();
                        lstk.Add(key);
                        SimpleKeys.Add(simple, lstk);
                    }
                }
            }

            bw = new BackgroundWorker();
            bw.DoWork += (sender, e) =>
            {
                Debug.WriteLine("백그라운드 인덱싱 시작");
                FindEachFolder(KnownFolders.MusicLibrary);
            };
            bw.RunWorkerAsync();
        }
        static BackgroundWorker bw = null;
        static void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        { 
            Working = true;
            if(ProgressReport != null)
            {
                ProgressReport(e.ProgressPercentage, "인덱스 생성 중");
            }
        }

        /// <summary>
        /// 라이브러리 키워드 지정
        /// </summary>
        /// <param name="song"></param>
        /// <returns></returns>
        private static string LibraryKeyword(Song song)
        {
            string artist = song.Artist.Name;
            string name = song.Name.Trim();
            name = CleanExt(name);

            string key = song.TrackNumber + "|" + name + "|" + artist.Trim() + "|" + song.Album.Name.Trim();
            return key;
        }

        private static string CleanExt(string name)
        {
            string iname = string.Copy(name).ToLower();
            if (iname.EndsWith(".mp3") || iname.EndsWith(".wma") || iname.EndsWith(".wav") || iname.EndsWith(".m4a"))
            {
                name = name.Substring(0, name.Length - 4);
            }
            return name;
        }

        static int itree = 0, maxtree = 0;
        private async static void FindEachFolder(StorageFolder sf, bool inside = false)
        {
            #region 라이브러리 검색
            itree++;
            maxtree++;
            Debug.WriteLine("폴더: "+sf.Path);
            //await Task.Delay(TimeSpan.FromSeconds(3));

            var lst = await sf.GetFilesAsync(CommonFileQuery.DefaultQuery);
            var fst = await sf.GetFoldersAsync(Windows.Storage.Search.CommonFolderQuery.DefaultQuery);
            if (fst != null)
            {
                for (int i = 0; i < fst.Count; i++)
                {
                    var f = fst[i];
                    if (!inside)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            Working = true;
                            if (ProgressReport != null)
                            {
                                ProgressReport(Math.Max(maxtree - itree, 0) / (double)maxtree, f.Name);
                            }
                        });
                    }
                    if (f.IsOfType(StorageItemTypes.Folder))
                    {
                        FindEachFolder(f, true);
                    }
                }
            }

            if (lst != null)
            {
                foreach (var r in lst)
                {
                    MusicProperties props = null;
                    if (r.ContentType.Contains("audio"))
                    {
                        props = await r.Properties.GetMusicPropertiesAsync();
                    }
                    else
                    {
                        if (r.FileType.ToLower() == ".lrc")
                        {
                            LoadingLRC.Add(r.Path);
                        }
                        continue;
                    }
                    if (props != null)
                    {
                        //Debug.WriteLine(r.Name+","+props.Title+","+props.Artist+","+props.AlbumArtist);

                        String file = null;
                        if (string.IsNullOrEmpty(props.Title))
                        {
                            file = r.DisplayName;
                        }
                        else
                        {
                            file = props.Title;
                        }

                        string[] contributingArtistsKey = { "System.Music.Artist" };
                        IDictionary<string, object> contributingArtistsProperty =
                            await props.RetrievePropertiesAsync(contributingArtistsKey);

                        string artist = null;
                        /*if (artist.ToLower().Contains("various"))
                        {*/
                        string[] artists = contributingArtistsProperty["System.Music.Artist"] as string[];
                        if (artists != null && artists.Length > 0)
                        {
                            artist = artists[0];
                        }
                        /*}*/

                        if (string.IsNullOrEmpty(artist))
                        {
                            if (string.IsNullOrEmpty(props.Artist))
                            {
                                artist = "Unknown";
                            }
                            else
                            {
                                artist = props.Artist;
                            }
                        }

                        string key = string.Format("{0}|{1}|{2}|{3}", props.TrackNumber, CleanExt(file.Trim()), artist.Trim(), /*props.Duration.ToString(@"hh\:mm\:ss"),*/ string.IsNullOrEmpty(props.Album) ? "Unknown" : props.Album.Trim());
                        if (LoadingKeys.ContainsKey(key))
                        {
                            LoadingKeys[key] = r.Path;
                            //Debug.WriteLine(key+": "+r.Path);
                        }
                        else
                        {
                            string nkey = string.Format("{0}|{1}", props.TrackNumber, CleanExt(file.Trim()));
                            if(SimpleKeys.ContainsKey(nkey))
                            {
                                for (int i = 0; i < SimpleKeys[nkey].Count; i++ )
                                {
                                    if (LoadingKeys[SimpleKeys[nkey][i]] == null)
                                    {
                                        LoadingKeys[SimpleKeys[nkey][i]] = r.Path;
                                        break;
                                    }
                                }
                            }
                            //querykey.Clear();
                        }
                    }
                    else
                    {
                        //Debug.WriteLine("! " + r.Name+","+r.Path);
                    }
                }
                itree--;
                if (itree <= 0)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            Working = false;
                            if (ProgressReport != null)
                            {
                                ProgressReport(0, string.Empty);
                            }
                        });

                    Debug.WriteLine("인덱싱 마무리 중");
                    Keys = new Dictionary<string, string>(LoadingKeys);
                    LoadingKeys.Clear();

                    LRCFound = new List<string>(LoadingLRC);
                    LoadingLRC.Clear();

                    await SaveMatchIndex();
                    await SaveLRCTable();

                    Debug.WriteLine("인덱싱 끝");
                     /*if (Doneinit != null)
                    {
                       foreach(var k in Keys)
                        {
                            if(k.Value == null)
                            {
                                Debug.WriteLine(k.Key+" ... not found.");
                            }
                        }


                        //Doneinit();
                    }*/
                }
            }

            #endregion
        }

        private static async Task SaveLRCTable()
        {
            MemoryStream sessionData = new MemoryStream();
            DataContractSerializer serializer = new
                        DataContractSerializer(typeof(List<string>));
            StorageFile file = await ApplicationData.Current.LocalCacheFolder.CreateFileAsync("lrctable.tmp", CreationCollisionOption.ReplaceExisting);


            serializer.WriteObject(sessionData, LRCFound);


            using (MemoryStream ms = new MemoryStream())
            {
                Compressor c = new Compressor(ms.AsOutputStream(), CompressAlgorithm.Mszip, 0);
                sessionData.Seek(0, SeekOrigin.Begin);
                byte[] bb = new byte[512];
                int lastread = sessionData.Read(bb, 0, bb.Length);
                while (lastread > 0)
                {
                    await c.WriteAsync(bb.AsBuffer(0, lastread));
                    lastread = sessionData.Read(bb, 0, bb.Length);
                }
                await c.FlushAsync();
                await c.FinishAsync();
                ms.Seek(0, SeekOrigin.Begin);
                using (Stream fileStream = await file.OpenStreamForWriteAsync())
                {
                    ms.CopyTo(fileStream);
                    await fileStream.FlushAsync();
                }
            }
        }

        private static async Task SaveMatchIndex()
        {
            MemoryStream sessionData = new MemoryStream();
            DataContractSerializer serializer = new
                        DataContractSerializer(typeof(Dictionary<string, string>));
            StorageFile file = await ApplicationData.Current.LocalCacheFolder.CreateFileAsync("lrcmatch.tmp", CreationCollisionOption.ReplaceExisting);


            serializer.WriteObject(sessionData, Keys);


            using (MemoryStream ms = new MemoryStream())
            {
                Compressor c = new Compressor(ms.AsOutputStream(), CompressAlgorithm.Mszip, 0);
                sessionData.Seek(0, SeekOrigin.Begin);
                byte[] bb = new byte[512];
                int lastread = sessionData.Read(bb, 0, bb.Length);
                while (lastread > 0)
                {
                    await c.WriteAsync(bb.AsBuffer(0, lastread));
                    lastread = sessionData.Read(bb, 0, bb.Length);
                }
                await c.FlushAsync();
                await c.FinishAsync();
                ms.Seek(0, SeekOrigin.Begin);
                Debug.WriteLine("Compressed: "+ms.Length);
                using (Stream fileStream = await file.OpenStreamForWriteAsync())
                {
                    ms.CopyTo(fileStream);
                    await fileStream.FlushAsync();
                }
            }
        }

        public static bool HasLocalLRC(Song csong)
        {
            string keyword = LibraryKeyword(csong);
            if (Keys.ContainsKey(keyword) && Keys[keyword] != null && Keys[keyword].Length > 5)
            {
                string mfile = Keys[keyword];
                string iname = string.Copy(mfile).ToLower();
                string lrcfile = null;
                if (iname.EndsWith(".mp3") || iname.EndsWith(".wma") || iname.EndsWith(".wav") || iname.EndsWith(".m4a"))
                {
                    lrcfile = mfile.Remove(mfile.Length - 4, 4);
                    lrcfile += ".lrc";
                }
                //string path = System.IO.Path.GetFullPath(mfile);

                return LRCFound.Contains(lrcfile);
            }
            return false;
        }

        public async static Task<List<LyricsItem>> ShowMetaLyrics(Song csong)
        {
            string keyword = LibraryKeyword(csong);
            if(Keys.ContainsKey(keyword))
            {
                try
                {
                    var sf = await StorageFile.GetFileFromPathAsync(Keys[keyword]);
                    var prop = await sf.Properties.GetMusicPropertiesAsync();
                    if (prop.Subtitle.Length > 0)
                    {
                        StringReader sr = new StringReader(prop.Subtitle);
                        List<LyricsItem> lst = new List<LyricsItem>();
                        string block = sr.ReadLine();
                        while(block != null)
                        {
                            lst.Add(new LyricsItem(TimeSpan.MinValue, block));
                            block = sr.ReadLine();
                        }
                        return lst;
                    }
                }
                catch
                {

                }
            }
            return null;
        }

        public async static Task<List<LyricsItem>> FillLRC(Song csong)
        {
            try
            {
                string keyword = LibraryKeyword(csong);
                if (Keys.ContainsKey(keyword) && Keys[keyword] != null && Keys[keyword].Length > 5)
                {
                    string mfile = Keys[keyword];
                    string iname = string.Copy(mfile).ToLower();
                    string lrcfile = null;
                    if (iname.EndsWith(".mp3") || iname.EndsWith(".wma") || iname.EndsWith(".wav") || iname.EndsWith(".m4a"))
                    {
                        lrcfile = mfile.Remove(mfile.Length - 4, 4);
                        lrcfile += ".lrc";
                    }
                    //string path = System.IO.Path.GetFullPath(mfile);
                    StorageFile lrcf = null;
                    try
                    {
                        lrcf = await StorageFile.GetFileFromPathAsync(lrcfile);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.GetType().ToString() + " LRC 오류: " + lrcfile);
                    }
                    if (lrcf != null)
                    {
                        using (IInputStream inStream = await lrcf.OpenSequentialReadAsync())
                        {
                            StreamReader stream = new StreamReader(inStream.AsStreamForRead());

                            List<LyricsItem> lstl = new List<LyricsItem>();
                            string cnt = stream.ReadToEnd();
                            int pos = cnt.IndexOf('[');
                            string timestr = "", content = "";
                            TimeSpan ts = TimeSpan.MinValue;
                            TimeSpan lts = TimeSpan.MinValue;
                            while (pos >= 0)
                            {
                                int por = cnt.IndexOf(']', pos);
                                bool skipneeded = false;
                                if (por != -1)
                                {
                                    char timestart = cnt[pos + 1];
                                    timestr = cnt.Substring(pos + 1, por - pos - 1).Replace('.', ':');
                                    if (timestart > 47 && timestart < 58)
                                    {
                                        string[] tsplit = timestr.Split(':');
                                        int[] ti = new int[3] { 0, 0, 0 };
                                        if (tsplit.Length == 3)
                                        {
                                            bool suc = false;
                                            suc = suc & int.TryParse(tsplit[0], out ti[0]);
                                            suc = suc & int.TryParse(tsplit[1], out ti[1]);
                                            suc = suc & int.TryParse(tsplit[2], out ti[2]);
                                        }
                                        ts = TimeSpan.FromMilliseconds(ti[2] * 10);
                                        ts = ts.Add(TimeSpan.FromSeconds(ti[1]));
                                        ts = ts.Add(TimeSpan.FromMinutes(ti[0]));
                                        if (content.Length > 0 && lts > TimeSpan.MinValue)
                                        {
                                            lstl.Add(new LyricsItem(lts, content));
                                            content = "";
                                            timestr = "";
                                            //Debug.WriteLine("{0},{1}", lts, content);
                                        }
                                    }
                                    else
                                    {
                                        if (!(timestr.Contains("ar:") || timestr.Contains("ti:") || timestr.Contains("al:")))
                                        {
                                            content += "[" + timestr + "]";
                                        }
                                        skipneeded = true;
                                    }
                                }

                                pos = cnt.IndexOf('[', pos + 1);
                                if (pos == -1) //마지막 싱크
                                {
                                    content += cnt.Substring(por + 1, cnt.Length - por - 1);
                                    if (content.Length > 0 && lts > TimeSpan.MinValue)
                                    {
                                        if (content[content.Length - 1] == '\n' || content[content.Length - 1] == '\r')
                                        {
                                            content = content.Remove(content.Length - 1, 1);
                                        }
                                        if (content[content.Length - 1] == '\n' || content[content.Length - 1] == '\r')
                                        {
                                            content = content.Remove(content.Length - 1, 1);
                                        }

                                        lstl.Add(new LyricsItem(ts, content));
                                        Debug.WriteLine("{0},{1}", ts, content);
                                    }
                                    break;
                                }

                                if (skipneeded)
                                {
                                    continue;
                                }

                                content += cnt.Substring(por + 1, pos - por - 1);
                                if (content.Length > 0)
                                {
                                    if (content[content.Length - 1] == '\n' || content[content.Length - 1] == '\r')
                                    {
                                        content = content.Remove(content.Length - 1, 1);
                                    }
                                    if (content[content.Length - 1] == '\n' || content[content.Length - 1] == '\r')
                                    {
                                        content = content.Remove(content.Length - 1, 1);
                                    }
                                }
                                lts = ts;
                            }

                            return lstl;
                        }
                    }
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                var lst = new List<LyricsItem>();
                lst.Add(new LyricsItem(TimeSpan.Zero, Res.LocalString.lyricserror + ": " + ex.GetType().ToString()));
                return lst;
            }
        }
    }
}

