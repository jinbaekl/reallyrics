﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace bnReal
{
    public class AlbumCustom
    {
        public Album InnerAlbum
        {
            get
            {
                return _album;
            }
        }
        private Album _album = null;

        public Brush Art
        {
            get
            {
                return _bu;
            }
        }
        private Brush _bu = null;

        public AlbumCustom(Album inside)
        {
            if (inside != null)
            {
                try
                {
                    _album = inside;
                    using(Stream st = inside.GetThumbnail())
                    {
                        if (st == null)
                        {
                            SolidColorBrush scb = new SolidColorBrush(Colors.Gray);
                            scb.Opacity = 0.75;
                            _bu = scb;
                        }
                        else
                        {
                            BitmapImage bi = new BitmapImage();
                            bi.CreateOptions = BitmapCreateOptions.DelayCreation;
                            bi.SetSource(st);

                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = bi;
                            ib.Opacity = 0.8;

                            _bu = ib;
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }
}
