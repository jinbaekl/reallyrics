﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Media;
using System.Windows.Markup;
using System.Globalization;
using System.Windows.Media;

namespace bnReal
{
    public partial class ArtistPage : PhoneApplicationPage
    {
        public ArtistPage()
        {
            InitializeComponent();
            this.Language = XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);
        }

        AlbumCollection ac = null;
        SongCollection sc = null;
        MediaLibrary ml = null;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ml = new MediaLibrary();
            if (NavigationContext.QueryString.ContainsKey("artist"))
            {
                int m = -1;
                int.TryParse(NavigationContext.QueryString["artist"], out m);
                if (m >= 0)
                {
                    Artist a = ml.Artists[m];
                    ac = a.Albums;
                    sc = a.Songs;
                    pvArtist.Title = a.Name;
                }
            }
            else if (NavigationContext.QueryString.ContainsKey("album"))
            {
                int m = -1;
                int.TryParse(NavigationContext.QueryString["album"], out m);
                if (m >= 0)
                {
                    Album a = ml.Albums[m];
                    sc = a.Songs;
                    pvArtist.Title = Res.LocalString.album;
                    piSong.Header = a.Name;
                }
                
            }
            else if (NavigationContext.QueryString.ContainsKey("genre"))
            {
                int m = -1;
                int.TryParse(NavigationContext.QueryString["genre"], out m);
                if (m >= 0)
                {
                    Genre a = ml.Genres[m];
                    sc = a.Songs;
                    pvArtist.Title = Res.LocalString.genre;
                    piSong.Header = a.Name;
                }
            }
            else if (NavigationContext.QueryString.ContainsKey("playlist"))
            {
                int m = -1;
                int.TryParse(NavigationContext.QueryString["playlist"], out m);
                if (m >= 0)
                {
                    Playlist a = ml.Playlists[m];
                    sc = a.Songs;
                    pvArtist.Title = Res.LocalString.playlist;
                    piSong.Header = a.Name;
                }
            }
            else if (NavigationContext.QueryString.ContainsKey("artist_txt"))
            {
                string s = NavigationContext.QueryString["artist_txt"];
                if (!string.IsNullOrEmpty(s))
                {
                    Artist a = ml.Artists.FirstOrDefault((f) => { return f.Name == s; });
                    if (a != null)
                    {
                        ac = a.Albums;
                        sc = a.Songs;
                        pvArtist.Title = a.Name;
                    }
                    else
                    {
                        if (NavigationService.CanGoBack)
                        {
                            NavigationService.GoBack();
                        }
                        return;
                    }
                }
            }
            else
            {
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
                return;
            }
        }

        private void pvArtist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tbNothing.Visibility = System.Windows.Visibility.Collapsed;
            PivotItem pi = e.AddedItems[0] as PivotItem;
            Grid gpi = null;

            foreach (PivotItem p in pvArtist.Items)
            {
                gpi = p.Content as Grid;
                if (gpi != null)
                {
                    if (gpi != null && gpi.Children.Count > 0)
                    {
                        if (gpi.Children[0] is LongListSelector)
                        {
                            gpi.Children.Clear();
                            break;
                        }
                    }
                }
            }

            if (pi != null)
            {
                gpi = pi.Content as Grid;
                if (gpi != null)
                {
                    if (gpi != null)
                    {
                        if (gpi.Children.Count == 0)
                        {
                            //gpi.Children.Clear();
                            gpi.Children.Add(llsSong);
                        }
                    }
                }
            }

            if (pvArtist.SelectedItem != null)
            {
                if ((pvArtist.SelectedItem as PivotItem).Name == "piAlbum" && ac != null)
                {
                    llsSong.ItemTemplate = this.Resources["AlbumItemTemplate"] as DataTemplate;
                    List<Album> lb = ac.ToList<Album>();
                    List<AlbumCustom> acb = new List<AlbumCustom>();
                    foreach (Album a in lb)
                    {
                        acb.Add(new AlbumCustom(a));
                    }
                    lb.Clear();
                    llsSong.ItemsSource = acb;
                    if (ac.Count == 0)
                    {
                        tbNothing.Visibility = System.Windows.Visibility.Visible;
                    }
                }
                else if (sc != null)
                {
                    llsSong.ItemTemplate = this.Resources["MusicItemTemplate"] as DataTemplate;
                    llsSong.ItemsSource = sc.ToList<Song>();
                    if (sc.Count == 0)
                    {
                        tbNothing.Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            object o = (sender as Button).DataContext;
            if (o == null)
            {
                //MediaPlayer.Play(ml.Songs);
                //MediaPlayer.IsShuffled = true;
            }
            else if (o is Song)
            {
                Song s = o as Song;
                List<Song> ls = sc.ToList();
                int i = ls.IndexOf(s);
                bool bs = MediaPlayer.IsShuffled;
                MediaPlayer.IsShuffled = false;
                MediaPlayer.Play(sc);
                MediaPlayer.Queue.ActiveSongIndex = i;
                MediaPlayer.IsShuffled = bs;
            }
            else if (o is AlbumCustom)
            {
                Album a = (o as AlbumCustom).InnerAlbum;
                MediaPlayer.Play(a.Songs);
            }
            else
            {
                return;
            }
            NavigationService.Navigate(new Uri("/NowPlaying.xaml", UriKind.Relative)); 
        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int cnt = VisualTreeHelper.GetChildrenCount(sender as DependencyObject);
            for (int i = 0; i < cnt; i++)
            {
                DependencyObject dO = VisualTreeHelper.GetChild(sender as DependencyObject, i);
                if (dO != null && dO is Button && (dO as Button).Name == "btnPlay")
                {
                    btnPlay_Click(dO, null);
                }
            }
        }

        private void llsSong_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                object o = e.AddedItems[0];
                if (o is Song)
                {
                    Song s = o as Song;
                    List<Song> ls = sc.ToList();
                    int i = ls.IndexOf(s);
                    bool bs = MediaPlayer.IsShuffled;
                    MediaPlayer.IsShuffled = false;
                    MediaPlayer.Play(sc);
                    MediaPlayer.Queue.ActiveSongIndex = i;
                    MediaPlayer.IsShuffled = bs;
                    NavigationService.Navigate(new Uri("/NowPlaying.xaml", UriKind.Relative)); 
                }
                else if (o is AlbumCustom)
                {
                    Album a = (o as AlbumCustom).InnerAlbum;
                    List<Album> ls = ml.Albums.ToList();
                    int i = ls.IndexOf(a);
                    NavigationService.Navigate(new Uri("/ArtistPage.xaml?album=" + i.ToString(), UriKind.Relative));
                }
            }
        }

        ProgressIndicator pi = new ProgressIndicator();
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            tbNothing.Visibility = System.Windows.Visibility.Collapsed;
            if (llsSong.ItemsSource == null)
            {
                pi.IsIndeterminate = true;
                pi.IsVisible = true;
                pi.Text = " ";
            }
            SystemTray.ProgressIndicator = pi;

            this.Dispatcher.BeginInvoke(() =>
            {
                if (NavigationContext.QueryString.ContainsKey("album") || NavigationContext.QueryString.ContainsKey("genre") || NavigationContext.QueryString.ContainsKey("playlist"))
                {
                    if (pvArtist.Items.Count == 2 && gdAlbum.Children.Count > 0)
                    {
                        gdAlbum.Children.Remove(llsSong);
                        gdSong.Children.Add(llsSong);
                        pvArtist.Items.RemoveAt(0);
                    }
                }

                pvArtist.SelectionChanged += pvArtist_SelectionChanged;
                if ((pvArtist.SelectedItem as PivotItem).Name == "piAlbum" && ac != null)
                {
                    llsSong.ItemTemplate = this.Resources["AlbumItemTemplate"] as DataTemplate;
                    if (ac.Count > 0)
                    {
                        List<Album> lb = ac.ToList<Album>();
                        List<AlbumCustom> acb = new List<AlbumCustom>();
                        foreach (Album a in lb)
                        {
                            acb.Add(new AlbumCustom(a));
                        }
                        lb.Clear();
                        llsSong.ItemsSource = acb;
                    }
                    else
                    {
                        if (pvArtist.Items.Count == 2 && gdAlbum.Children.Count > 0)
                        {
                            gdAlbum.Children.Remove(llsSong);
                            gdSong.Children.Add(llsSong);
                            pvArtist.Items.RemoveAt(0);
                        }
                        else if(sc.Count == 0)
                        {
                            tbNothing.Visibility = System.Windows.Visibility.Visible;
                        }
                    }
                }
                else if(sc != null)
                {
                    llsSong.ItemTemplate = this.Resources["MusicItemTemplate"] as DataTemplate;
                    llsSong.ItemsSource = sc.ToList<Song>();
                    if (sc.Count == 0)
                    {
                        tbNothing.Visibility = System.Windows.Visibility.Visible;
                    }
                }
                llsSong.SelectionChanged += llsSong_SelectionChanged;
            });
        }

        private void llsSong_Loaded(object sender, RoutedEventArgs e)
        {
            pi.IsIndeterminate = false;
            pi.IsVisible = false;
            pi.Text = "";
            SystemTray.ProgressIndicator = null;
        }

        private void pvArtist_Loaded(object sender, RoutedEventArgs e)
        {
            
        }


    }
}