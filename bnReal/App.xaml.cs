﻿#define AD

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using System.IO;
using System.Diagnostics;

namespace bnReal
{
    public partial class App : Application
    {
        /// <summary>
        /// 전화 응용 프로그램의 루트 프레임에 간단하게 액세스할 수 있습니다.
        /// </summary>
        /// <returns>전화 응용 프로그램의 루트 프레임입니다.</returns>
        public static PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Application 개체의 생성자입니다.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // 전화 관련 초기화
            InitializePhoneApplication();

            // 디버깅하는 동안 그래픽 프로파일링 정보를 표시합니다.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // 현재 프레임 속도 카운터를 표시합니다.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // 각 프레임에서 다시 그려지는 응용 프로그램의 영역을 표시합니다.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // 비프로덕션 분석 시각화 모드를 설정합니다.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // 주의:- 디버그 모드에서만 사용합니다. 사용자 유휴 검색을 해제하는 응용 프로그램은 사용자가 전화를 사용하지 않을 경우에도
                // 계속 실행되어 배터리 전원을 소모합니다.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }

        // 시작 메뉴 등에서 응용 프로그램을 시작할 때 실행할 코드입니다.
        // 이 코드는 응용 프로그램이 다시 활성화될 때는 실행되지 않습니다.
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            FrameworkDispatcher.Update();
            ticklauch = DateTime.Now;
        }

        // 응용 프로그램이 활성화(포그라운드로 이동)될 때 실행할 코드입니다.
        // 이 코드는 응용 프로그램이 처음 시작될 때는 실행되지 않습니다.
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            FrameworkDispatcher.Update();
        }

        // 응용 프로그램이 비활성화(백그라운드로 전송)될 때 실행할 코드입니다.
        // 이 코드는 응용 프로그램이 닫힐 때는 실행되지 않습니다.
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // 응용 프로그램이 닫힐 때(예: 사용자가 [뒤로]를 누르는 경우) 실행할 코드입니다.
        // 이 코드는 응용 프로그램이 비활성화될 때는 실행되지 않습니다.
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // 탐색이 실패할 때 실행할 코드입니다.
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // 탐색이 실패했습니다. 중단하고 디버거를 실행합니다.
                System.Diagnostics.Debugger.Break();
            }
        }

        // 처리되지 않은 예외에 대해 실행할 코드입니다.
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            //광고 관련
            if (e != null)
            {
                Exception exception = e.ExceptionObject;
                if ((exception is System.Xml.XmlException || exception is NullReferenceException) && exception.ToString().ToUpper().Contains("INNERACTIVE"))
                {
                    Debug.WriteLine("Handled Inneractive exception {0}", exception);
                    e.Handled = true;
                    return;
                }
                else if (exception is NullReferenceException && exception.ToString().ToUpper().Contains("SOMA"))
                {
                    Debug.WriteLine("Handled Smaato null reference exception {0}", exception);
                    e.Handled = true;
                    return;
                }
                else if ((exception is System.IO.IOException || exception is NullReferenceException) && exception.ToString().ToUpper().Contains("GOOGLE"))
                {
                    Debug.WriteLine("Handled Google exception {0}", exception);
                    e.Handled = true;
                    return;
                }
                else if (exception is ObjectDisposedException && exception.ToString().ToUpper().Contains("MOBFOX"))
                {
                    Debug.WriteLine("Handled Mobfox exception {0}", exception);
                    e.Handled = true;
                    return;
                }
                else if ((exception is NullReferenceException || exception is System.Windows.Markup.XamlParseException) && exception.ToString().ToUpper().Contains("MICROSOFT.ADVERTISING"))
                {
                    Debug.WriteLine("Handled Microsoft.Advertising exception {0}", exception);
                    e.Handled = true;
                    return;
                }

            }
            // APP SPECIFIC HANDLING HERE

            SaveErrorLog(e.ExceptionObject);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                // 처리되지 않은 예외가 발생했습니다. 중단하고 디버거를 실행합니다.
                System.Diagnostics.Debugger.Break();
            }
        }

        public static void SaveErrorLog(Exception ex)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf != null)
                {
                    using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Append,
                        isf))
                    {
                        if (isfs.CanWrite)
                        {
                            using (StreamWriter sw = new StreamWriter(isfs))
                            {
                                sw.WriteLine();
                                sw.WriteLine("=== {1} on {0} ===", DateTime.Now, ex.GetType().ToString());
                                sw.WriteLine(ex.Message);
                                sw.WriteLine();
                                sw.WriteLine(ex.StackTrace);
                                sw.WriteLine();
                                if (ex.InnerException != null)
                                {
                                    sw.WriteLine("Internal Error: {0}", ex.InnerException.GetType().ToString());
                                    sw.WriteLine(ex.InnerException);
                                }
                                sw.WriteLine();
                                sw.WriteLine("== Current state ==");
                                sw.WriteLine("Apptick: {0}", (DateTime.Now - ticklauch).ToString("g"));
                                try
                                {
                                    if (RootFrame != null)
                                    {
                                        sw.WriteLine(RootFrame.CurrentSource.OriginalString);
                                    }
                                }
                                catch (Exception exe) { sw.WriteLine("Unknown as following error: " + exe.ToString()); }
                                sw.WriteLine();
                                sw.WriteLine("== Mem ==");
                                sw.WriteLine("Limit: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationMemoryUsageLimit / 1024.0 / 1024.0);
                                sw.WriteLine("Peak: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationPeakMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Now: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.ApplicationCurrentMemoryUsage / 1024.0 / 1024.0);
                                sw.WriteLine("Total: {0}MB",
                                    Microsoft.Phone.Info.DeviceStatus.DeviceTotalMemory / 1024.0 / 1024.0);
                                sw.WriteLine();
                                sw.WriteLine("== DeviceInfo ==");
                                sw.WriteLine("Manufacture: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceManufacturer);
                                sw.WriteLine("Name: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceName);
                                sw.WriteLine("App: {0}", System.Reflection.Assembly.GetExecutingAssembly().FullName);
                                sw.WriteLine("Devicever: {0}", Microsoft.Phone.Info.DeviceStatus.DeviceFirmwareVersion);
                                sw.WriteLine("OS: {0} {1}", Environment.OSVersion.Platform, Environment.OSVersion.Version);
                                sw.WriteLine("Runtime: {0}", Environment.Version);
                            }
                        }
                    }
                }
            }
        }

        #region 전화 응용 프로그램 초기화

        // 이중 초기화를 사용하지 않습니다.
        private bool phoneApplicationInitialized = false;
        private static DateTime ticklauch;

        // 이 메서드에 추가 코드를 추가하지 않습니다.
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // 프레임을 만들지만 프레임을 아직 RootVisual로 설정하지 않습니다. 이렇게 하면
            // 응용 프로그램이 렌더링할 준비가 될 때까지 시작 화면이 활성 상태를 유지합니다.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigating += RootFrame_Navigating;
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // 탐색 오류를 처리합니다.
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // 다시 초기화하지 않습니다.
            phoneApplicationInitialized = true;
        }

        bool isReset = false;
        void RootFrame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            //Debug.WriteLine("{0}, {1}",e.NavigationMode, e.Uri, e.IsNavigationInitiator, e.IsCancelable);
            if(e.NavigationMode == NavigationMode.Reset)
            {
                isReset = true;
            }
            else if(isReset)
            {
                isReset = false;
                if(e.NavigationMode == NavigationMode.New)
                {
                    e.Cancel = true;
                }
            }
        }

        // 이 메서드에 추가 코드를 추가하지 않습니다.
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // 응용 프로그램을 렌더링하도록 루트 Visual을 설정합니다.
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // 더 이상 필요하지 않으므로 이 처리기를 제거합니다.
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}