﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace bnReal
{
    public class LyricsRecord
    {
        public string LyricID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Duration { get; set; }
        public string Desc { get; set; }
        public SolidColorBrush Bar { get; set; }

        public LyricsRecord()
        {
        }

        public LyricsRecord(string artist, string title, string desc, string duration, int up, int down, string id)
        {
            this.Artist = artist;
            this.Title = title;
            this.Desc = desc;
            this.Duration = duration;
            this.LyricID = id;
            if(up > down)
            {
                this.Bar = (SolidColorBrush)App.Current.Resources["PhoneAccentBrush"];
            }
            else if (up < down)
            {
                Color acc = (Color)App.Current.Resources["PhoneAccentColor"];
                Color white = Colors.White;
                this.Bar = new SolidColorBrush(Color.FromArgb(255, (byte)(white.R - acc.R), (byte)(white.G - acc.G), (byte)(white.B - acc.B)));                
            }
            else
            {
                this.Bar = new SolidColorBrush(Colors.Gray);
            }
        }

        public override bool Equals(object obj)
        {
            LyricsRecord si = obj as LyricsRecord;
            if (si == null)
            {
                return false;
            }
            if (si.LyricID == this.LyricID)
            {
                return true;
            }
            return false;
        }
    }
}
