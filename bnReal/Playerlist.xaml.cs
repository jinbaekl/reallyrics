﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Media;
using System.Globalization;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System;

namespace bnReal
{
    public class SongItem : INotifyPropertyChanged
    {
        // Create the OnPropertyChanged method to raise the event
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public string Name { get; set; }
        public string Artist { get; set; }
        public string Jaso { get; set; }
        public int Num { get; set; }

        private double _selected;
        public double Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnPropertyChanged("HighlightBackgroundColor");
            }
        }

    }

    public partial class Playerlist : PhoneApplicationPage
    {
        public static WriteableBitmap wb = null;
        bool loaded = false;
        ObservableCollection<SongItem> ocSearch = new ObservableCollection<SongItem>();
        public Playerlist()
        {
            InitializeComponent();
            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);

            (this.ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.libsearchbtn;
        }

        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (MediaPlayer.Queue.Count == 0 && NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
            else
            {
                if (!loaded)
                {
                    if (wb != null)
                    {
                        ImageBrush ib = new ImageBrush();
                        ib.ImageSource = wb;
                        ib.Opacity = 0.3;
                        LayoutRoot.Background = ib;
                    }
                    spLoading.Visibility = System.Windows.Visibility.Visible;
                    tbLoading.Text = string.Format(Res.LocalString.loadnowplay, MediaPlayer.Queue.Count);
                    pgLoading.IsIndeterminate = true;
                }
            }
            base.OnNavigatedTo(e);
        }

        private void RefreshList()
        {
            if (MediaPlayer.Queue.ActiveSongIndex == -1)
            {
                MediaPlayer.Resume();
                MediaPlayer.Pause();
            }

            lbNow.SelectionChanged -= lbNow_SelectionChanged;
            if(!(MediaPlayer.Queue.Count > 0 && StoredMem.mq.Count == MediaPlayer.Queue.Count && MediaPlayer.Queue[0].Name == StoredMem.mq[0].Name))
            {   
                StoredMem.mq.Clear();
                for (int i = 0; i < MediaPlayer.Queue.Count; i++)
                {
                    Song s = MediaPlayer.Queue[i];
                    string t = s.Name;
                    string a = string.Empty;
                    if (s.Artist != null)
                    {
                        a = s.Artist.Name;
                    }
                    StoredMem.mq.Add(new SongItem() { Name = t, Artist = a, Jaso = HangulJaso.Seperate(t), Num = StoredMem.mq.Count });
                }
            }
            lbNow.ItemsSource = StoredMem.mq;
            lbNow.UpdateLayout();
            if (StoredMem.mq.Count > 0 && StoredMem.mq.Count > MediaPlayer.Queue.ActiveSongIndex && MediaPlayer.Queue.ActiveSongIndex >= 0)
            {
                lbNow.ScrollTo(StoredMem.mq[MediaPlayer.Queue.Count - 1]);
                lbNow.UpdateLayout();
                lbNow.SelectedItem = StoredMem.mq[MediaPlayer.Queue.ActiveSongIndex];
                foreach (var item in StoredMem.mq)
                {
                    item.Selected = 0;
                }
                foreach (var item in ocSearch)
                {
                    item.Selected = 0;
                }
                StoredMem.mq[MediaPlayer.Queue.ActiveSongIndex].Selected = 1.0;
                lbNow.ScrollTo(StoredMem.mq[Math.Max(0,MediaPlayer.Queue.ActiveSongIndex - 3)]);
            }
            lbNow.ApplyTemplate();
            lbNow.UpdateLayout();
            lbNow.SelectionChanged += lbNow_SelectionChanged;
        }

        private void lbNow_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(e.AddedItems.Count > 0)
            {
                foreach(var item in StoredMem.mq)
                {
                    item.Selected = 0;
                }
                foreach (var item in ocSearch)
                {
                    item.Selected = 0;
                }

                SongItem m = e.AddedItems[0] as SongItem;
                m.Selected = 1.0;
                /*MediaPlayer.Queue.ActiveSongIndex = m.Num;
                if (MediaPlayer.State != MediaState.Playing)
                {
                    MediaPlayer.Resume();
                }
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }*/
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshList();
        }

        private void lbNow_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Background = null;
            SystemTray.IsVisible = true;
            spLoading.Visibility = System.Windows.Visibility.Collapsed;
            pgLoading.IsIndeterminate = false;
            loaded = true;
        }

        private void lbNow_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SongItem m = (sender as LongListSelector).SelectedItem as SongItem;
            if (m != null)
            {
                MediaPlayer.Queue.ActiveSongIndex = m.Num;
                if (MediaPlayer.State != MediaState.Playing)
                {
                    MediaPlayer.Resume();
                }
                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
        }

        private void abSearch_Click(object sender, System.EventArgs e)
        {
            if (gdSearch.Visibility == System.Windows.Visibility.Visible)
            {
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                ocSearch.Clear();
                tboxSearch.Text = "";
                this.ApplicationBar.IsVisible = true;
                lbNow.Focus();
            }
            else
            {
                tboxSearch.Text = "";
                gdSearch.Visibility = System.Windows.Visibility.Visible;
                lbSearch.ItemsSource = ocSearch;
                this.ApplicationBar.IsVisible = false;
                tboxSearch.Focus();
            }
        }

        private void tboxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string txt = tboxSearch.Text.ToLower();
            if(txt == "")
            {
                ocSearch.Clear();
            }
            else if(StoredMem.mq != null)
            {
                var lst = StoredMem.mq.FindAll(a => { 
                    return a.Name.ToLower().Contains(txt) || a.Jaso.ToLower().Contains(HangulJaso.Seperate(txt)) || a.Artist.ToLower().Contains(txt);
                });
                lbSearch.ItemsSource = null;
                ocSearch.Clear();
                foreach (var l in lst)
                {
                    ocSearch.Add(l);
                }
                lbSearch.ItemsSource = ocSearch;
            }
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gdSearch.Visibility == System.Windows.Visibility.Visible)
            {
                e.Cancel = true;
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                ocSearch.Clear();
                tboxSearch.Text = "";
                this.ApplicationBar.IsVisible = true;
                lbNow.Focus();
            }
        }

        private void tboxSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            if(tboxSearch.Text.Length == 0)
            {
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                ocSearch.Clear();
                tboxSearch.Text = "";
                this.ApplicationBar.IsVisible = true;
                lbNow.Focus();
            }
        }
    }
}