﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Media;
using System.Globalization;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Xna.Framework;
using System.Windows.Markup;
using System.IO.IsolatedStorage;
using Microsoft.Devices;
using Microsoft.Phone.Tasks;
using System.Reflection;
using System.Diagnostics;

namespace bnReal
{
    public partial class Library : PhoneApplicationPage
    {
        MediaLibrary ml = new MediaLibrary();
        DispatcherTimer dtTimer = new DispatcherTimer();
        bool fired = false;
        ProgressIndicator pi = new ProgressIndicator();
        public Library()
        {
            InitializeComponent();
            this.Language = XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name);
            this.ApplicationBar.IsVisible = false;

            if (this.ApplicationBar.Buttons.Count > 0)
            {
                (this.ApplicationBar.Buttons[0] as ApplicationBarIconButton).Text = Res.LocalString.nowplay;
            }

            if(lpUpdate.Items.Count == 3)
            {
                lpUpdate.Items[0] = Res.LocalString.opt_lpup0;
                lpUpdate.Items[1] = Res.LocalString.opt_lpup1;
                lpUpdate.Items[2] = Res.LocalString.opt_lpup2;
            }
        }

        void dtTimer_Tick(object sender, EventArgs e)
        {
            RefreshNowRealtime();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (ml == null)
            {
                ml = new MediaLibrary();
            }
            base.OnNavigatedTo(e);
            dtTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            dtTimer.Tick += dtTimer_Tick;
            RefreshNowRealtime();
            if (!dtTimer.IsEnabled)
            {
                dtTimer.Start();
            }
            fired = false;

            if (ml.Songs.Count > 0 && lstQuick.Items.Count == 0)
            {
                lstQuick.Items.Add(Res.LocalString.artist);
                lstQuick.Items.Add(Res.LocalString.album);
                lstQuick.Items.Add(Res.LocalString.song);
                lstQuick.Items.Add(Res.LocalString.genre);
                lstQuick.Items.Add(Res.LocalString.playlist);
                lstQuick.Items.Add(Res.LocalString.settings);
            }

            llsSong.SelectedItem = null;
            fired = false;

            if (e.NavigationMode == NavigationMode.New)
            {
                LocalLyrics.Doneinit = new Action(() =>
                {
                    //
                });
                LocalLyrics.ProgressReport = (i, s) =>
                {
                    if (i == 0)
                    {
                        SystemTray.SetProgressIndicator(this, null);
                        pi.IsVisible = false;
                        btnSearchLib.IsEnabled = true;
                    }
                    else
                    {
                        SystemTray.SetProgressIndicator(this, pi);
                        pi.IsVisible = true;
                        pi.Value = i;
                        pi.Text = s;
                        btnSearchLib.IsEnabled = false;
                        SystemTray.IsVisible = true;
                    }
                };
                LocalLyrics.Initialize(ml);
            }

            if (MediaPlayer.Queue != null)
            {
                if (MediaPlayer.Queue.ActiveSong != null)
                {
                    this.ApplicationBar.IsVisible = pvSong.SelectedIndex != 0;
                    return;
                }
            }
            this.ApplicationBar.IsVisible = false;

            
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (al != null)
            {
                al = null;
            }
            if (bl != null)
            {
                bl = null;
            }
            if (cl != null)
            {
                cl = null;
            }
            if (dl != null)
            {
                dl = null;
            }
            if (el != null)
            {
                el = null;
            }

            if (dtTimer.IsEnabled)
            {
                dtTimer.Stop();
            }
            ml.Dispose();
            ml = null;
        }

        Song se = null;
        private void RefreshNowRealtime()
        {
            try
            {
                if (MediaPlayer.Queue != null)
                {
                    if (MediaPlayer.Queue.ActiveSong != null)
                    {
                        Song sn = MediaPlayer.Queue.ActiveSong;
                        if (sn != se)
                        {
                            gdNow.Background = new SolidColorBrush(Colors.Gray);
                            if (sn.Album != null)
                            {
                                try
                                {
                                    using (Stream s = sn.Album.GetAlbumArt())
                                    {
                                        if (s == null)
                                        {
                                            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                            {
                                                using (IsolatedStorageFileStream isfs = isf.OpenFile(@"\Shared\ShellContent\album.img", FileMode.Create))
                                                {
                                                    isfs.Flush();
                                                }
                                            }
                                            ShellTile st = ShellTile.ActiveTiles.First();
                                            st.Update(new StandardTileData()
                                            {
                                                BackBackgroundImage = new Uri("isostore:/Shared/ShellContent/album.img",
                                                    UriKind.Absolute)
                                            });
                                        }
                                        else
                                        {
                                            using (MemoryStream ms = new MemoryStream())
                                            {
                                                s.CopyTo(ms);
                                                ms.Position = 0;
                                                BitmapImage bi = new BitmapImage() { CreateOptions = BitmapCreateOptions.None };
                                                bi.SetSource(ms);
                                                gdNow.Background = new ImageBrush() { ImageSource = bi, Stretch = Stretch.UniformToFill, Opacity = 0.7 };

                                                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                                {
                                                    using (IsolatedStorageFileStream isfs = isf.OpenFile(@"\Shared\ShellContent\album.img", FileMode.Create))
                                                    {
                                                        ms.CopyTo(isfs);
                                                        isfs.Flush();
                                                    }
                                                }
                                                ShellTile st = ShellTile.ActiveTiles.First();
                                                st.Update(new StandardTileData()
                                                {
                                                    BackBackgroundImage = new Uri("isostore:/Shared/ShellContent/album.img",
                                                        UriKind.Absolute)
                                                });
                                            }
                                        }
                                    }
                                }
                                catch
                                {

                                }
                            }
                            se = sn;
                        }
                        if (MediaPlayer.State == MediaState.Playing)
                        {
                            tbTime.Text = MediaPlayer.PlayPosition.ToString("m':'ss");
                        }
                        else
                        {
                            tbTime.Text = MediaPlayer.State == MediaState.Paused ? Res.LocalString.paused : Res.LocalString.stopped;
                        }
                        tbTitle.Text = sn.Name;

                        if (btnNow.Visibility == System.Windows.Visibility.Collapsed)
                        {
                            btnNow.Visibility = System.Windows.Visibility.Visible;
                            tbNosong.Visibility = System.Windows.Visibility.Collapsed;
                            tbNostore.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        return;
                    }
                }
            }
            catch { }
            /*if (MediaHistory.Instance.NowPlaying != null)
            {
                MediaHistoryItem mhi = MediaHistory.Instance.NowPlaying;
                if (mhi.ImageStream != null)
                {
                    using (Stream s = mhi.ImageStream)
                    {
                        if (s != null)
                        {
                            BitmapImage bi = new BitmapImage() { CreateOptions = BitmapCreateOptions.None };
                            bi.SetSource(s);
                            gdNow.Background = new ImageBrush() { ImageSource = bi, Stretch = Stretch.UniformToFill, Opacity = 0.7 };
                        }
                    }
                }
                tbTime.Text = MediaPlayer.State == MediaState.Paused ? "일시 정지" : "정지";
                tbTitle.Text = mhi.Title;
                if (btnNow.Visibility == System.Windows.Visibility.Collapsed)
                {
                    btnNow.Visibility = System.Windows.Visibility.Visible;
                    tbNosong.Visibility = System.Windows.Visibility.Collapsed;
                }
                return;
            }*/
            if (btnNow.Visibility == System.Windows.Visibility.Visible)
            {
                btnNow.Visibility = System.Windows.Visibility.Collapsed;
            }
            if(btnNow.Visibility == System.Windows.Visibility.Collapsed)
            {
                if (ml.Songs.Count > 0)
                {
                    tbNosong.Visibility = System.Windows.Visibility.Visible;
                    tbNostore.Visibility = System.Windows.Visibility.Collapsed;

                    if (pvSong.Items.Count <= 2)
                    {
                        pvSong.Items.Add(piArtist);
                        pvSong.Items.Add(piAlbum);
                        pvSong.Items.Add(piSong);
                        pvSong.Items.Add(piGenre);
                        pvSong.Items.Add(piPlaylist);
                    }
                }
                else
                {
                    tbNosong.Visibility = System.Windows.Visibility.Collapsed;
                    tbNostore.Visibility = System.Windows.Visibility.Visible;

                    if(pvSong.Items.Count != 2)
                    {
                        pvSong.Items.Remove(piArtist);
                        pvSong.Items.Remove(piAlbum);
                        pvSong.Items.Remove(piSong);
                        pvSong.Items.Remove(piGenre);
                        pvSong.Items.Remove(piPlaylist);
                    }
                }
            }

            ShellTile std = ShellTile.ActiveTiles.First();
            std.Update(new StandardTileData()
            {
                BackBackgroundImage = new Uri("/blank.img", UriKind.Relative),
                Count = 0
            });
        }

        private void btnNow_Click(object sender, RoutedEventArgs e)
        {
            GoToPlayer();
        }

        private void GoToPlayer()
        {
            NavigationService.Navigate(new Uri("/NowPlaying.xaml", UriKind.Relative));
        }

        System.Collections.IList al, bl, cl, dl, el = null;
        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PivotItem pi = e.AddedItems[0] as PivotItem;
            Grid gpi = null;
            //bool isDeleted = false;
            foreach (PivotItem p in pvSong.Items)
            {
                gpi = p.Content as Grid;
                if (gpi != null)
                {
                    if (gpi != null && gpi.Children.Count > 0)
                    {
                        if (gpi.Children[0] is LongListSelector)
                        {
                            gpi.Children.Clear();
                            break;
                        }
                    }
                }
            }

            if (pi != null)
            {
                gpi = pi.Content as Grid;
                if (gpi != null)
                {
                    if (gpi != null)
                    {
                        if (gpi.Children.Count == 0)
                        {
                            //gpi.Children.Clear();
                            gpi.Children.Add(llsSong);
                        }
                    }
                }
            }

            if (pvSong.SelectedItem != null)
            {
                switch ((pvSong.SelectedItem as PivotItem).Name)
                {
                    case "piArtist":
                        llsSong.ItemTemplate = this.Resources["ArtistItemTemplate"] as DataTemplate;
                        if (al == null)
                        {
                            al = AlphaKeyGroup<Artist>.CreateGroups(ml.Artists,
                    CultureInfo.CurrentUICulture, (a) => { return a.Name; }, true);
                        }
                        llsSong.ItemsSource = al;
                        break;
                    case "piAlbum":
                        llsSong.ItemTemplate = this.Resources["AlbumItemTemplate"] as DataTemplate;
                        if (bl == null)
                        {
                            List<AlbumCustom> lac = new List<AlbumCustom>();
                            foreach (Album a in ml.Albums)
                            {
                                lac.Add(new AlbumCustom(a));
                            }

                            bl = AlphaKeyGroup<AlbumCustom>.CreateGroups(lac,
                    CultureInfo.CurrentUICulture, (a) => { return a.InnerAlbum.Name; }, true);
                            lac.Clear();
                        }
                        llsSong.ItemsSource = bl;
                        break;
                    case "piSong":
                        llsSong.ItemTemplate = this.Resources["MusicItemTemplate"] as DataTemplate;
                        if (cl == null)
                        {
                            cl = AlphaKeyGroup<Song>.CreateGroups(ml.Songs,
                    CultureInfo.CurrentUICulture, (a) => { return a.Name; }, true);
                        }
                        llsSong.ItemsSource = cl;
                        break;
                    case "piGenre":
                        llsSong.ItemTemplate = this.Resources["ArtistItemTemplate"] as DataTemplate;
                        if (dl == null)
                        {
                            dl = AlphaKeyGroup<Genre>.CreateGroups(ml.Genres,
                    CultureInfo.CurrentUICulture, (a) => { return a.Name; }, true);
                        }
                        llsSong.ItemsSource = dl;
                        break;
                    case "piPlaylist":
                        llsSong.ItemTemplate = this.Resources["ArtistItemTemplate"] as DataTemplate;
                        if (el == null)
                        {
                            el = AlphaKeyGroup<Playlist>.CreateGroups(ml.Playlists,
                    CultureInfo.CurrentUICulture, (a) => { return a.Name; }, true);
                        }
                        llsSong.ItemsSource = el;
                        break;
                    case "piSettings":
                        var assembly = Assembly.GetExecutingAssembly().FullName;
                        string ver = assembly.Split('=')[1].Split(',')[0];
                        tbVersion.Text = string.Format(Res.LocalString.version, ver);
                        using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                        {
                            if (isf != null)
                            {
                                hbError.Visibility = isf.FileExists("exception.txt") ? Visibility.Visible : Visibility.Collapsed;
                            }
                        }
                        tgScreen.Checked -= tgScreen_Checked;
                        tgScreen.Unchecked -= tgScreen_Checked;
                        lpUpdate.SelectionChanged -= lpUpdate_SelectionChanged;
                        long bgsize = 0;
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("bgsize"))
                        {
                            bgsize = (long)IsolatedStorageSettings.ApplicationSettings["bgsize"];
                            tbBgsize.Text = string.Format("{0} MB", Math.Round(bgsize / 1024.0 / 1024.0, 2));
                        }
                        long lcsize = 0;
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("lcsize"))
                        {
                            lcsize = (long)IsolatedStorageSettings.ApplicationSettings["lcsize"];
                            tbLcsize.Text = string.Format("{0} MB", Math.Round(lcsize / 1024.0 / 1024.0, 2));
                        }
                        bool? screen = false, live = true, full = false, jap = CultureInfo.CurrentCulture.Name == "ja-JP";
                        int bg = 0;
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("idle"))
                        {
                            screen = IsolatedStorageSettings.ApplicationSettings["idle"] as bool?;
                        }
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("bg") && IsolatedStorageSettings.ApplicationSettings["bg"] is int)
                        {
                            bg = (int)IsolatedStorageSettings.ApplicationSettings["bg"];
                        }
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("live"))
                        {
                            live = IsolatedStorageSettings.ApplicationSettings["live"] as bool?;
                        }
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("full"))
                        {
                            full = IsolatedStorageSettings.ApplicationSettings["full"] as bool?;
                        }
                        if (IsolatedStorageSettings.ApplicationSettings.Contains("jap"))
                        {
                            jap = IsolatedStorageSettings.ApplicationSettings["jap"] as bool?;
                        }
                        tgScreen.IsChecked = screen;
                        tgLive.IsChecked = live;
                        tgFull.IsChecked = full;
                        tgJapan.IsChecked = jap;
                        lpUpdate.SelectedIndex = bg;
                        lpUpdate.SelectionChanged += lpUpdate_SelectionChanged;
                        tgScreen.Checked += tgScreen_Checked;
                        tgScreen.Unchecked += tgScreen_Checked;
                        tgLive.Checked += tgScreen_Checked;
                        tgLive.Unchecked += tgScreen_Checked;
                        tgFull.Checked += tgScreen_Checked;
                        tgFull.Unchecked += tgScreen_Checked;
                        tgJapan.Checked += tgScreen_Checked;
                        tgJapan.Unchecked += tgScreen_Checked;
                        break;
                }
            }

            if (MediaPlayer.Queue != null)
            {
                if (MediaPlayer.Queue.ActiveSong != null)
                {
                    this.ApplicationBar.IsVisible = pvSong.SelectedIndex != 0;
                    return;
                }
            }
            this.ApplicationBar.IsVisible = false;
        }

        void lpUpdate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListPicker lp = sender as ListPicker;
            if(lp != null)
            {
                if (lp.Name == "lpUpdate")
                {
                    IsolatedStorageSettings.ApplicationSettings["bg"] = lp.SelectedIndex;
                }
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        void tgScreen_Checked(object sender, RoutedEventArgs e)
        {
            ToggleSwitch ts = sender as ToggleSwitch;
            if (ts != null)
            {
                if (ts.Name == "tgScreen")
                {
                    IsolatedStorageSettings.ApplicationSettings["idle"] = tgScreen.IsChecked;
                }
                else if (ts.Name == "tgLive")
                {
                    IsolatedStorageSettings.ApplicationSettings["live"] = tgLive.IsChecked;
                }
                else if (ts.Name == "tgFull")
                {
                    IsolatedStorageSettings.ApplicationSettings["full"] = tgFull.IsChecked;
                }
                else if (ts.Name == "tgJapan")
                {
                    IsolatedStorageSettings.ApplicationSettings["jap"] = tgJapan.IsChecked;
                }
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            object o = (sender as Button).DataContext;
            if (o == null)
            {
                //MediaPlayer.Play(ml.Songs);
                //MediaPlayer.IsShuffled = true;
            }
            else if(o is Song)
            {
                Song s = o as Song;
                List<Song> ls = ml.Songs.ToList();
                int i = ls.IndexOf(s);
                bool bs = MediaPlayer.IsShuffled;
                MediaPlayer.IsShuffled = false;
                MediaPlayer.Play(ml.Songs);
                MediaPlayer.Queue.ActiveSongIndex = i;
                MediaPlayer.IsShuffled = bs;
            }
            else if (o is Artist)
            {
                Artist a = o as Artist;
                if (a.Songs.Count > 0)
                {
                    MediaPlayer.Play(a.Songs);
                }
                else
                {
                    return;
                }
            }
            else if (o is AlbumCustom)
            {
                Album a = (o as AlbumCustom).InnerAlbum;
                MediaPlayer.Play(a.Songs);

            }
            else if (o is Playlist)
            {
                Playlist p = o as Playlist;
                MediaPlayer.Play(p.Songs);
            }
            else if (o is Genre)
            {
                Genre g = o as Genre;
                MediaPlayer.Play(g.Songs);
            }
            else
            {
                return;
            }
            GoToPlayer();
        }

        private void llsSong_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems.Count > 0 && !fired)
                {
                    fired = true;
                    object o = e.AddedItems[0];
                    if (o is Song)
                    {
                        Song s = o as Song;
                        List<Song> ls = ml.Songs.ToList();
                        int i = ls.IndexOf(s);
                        if (i >= 0)
                        {
                            bool bs = MediaPlayer.IsShuffled;
                            MediaPlayer.IsShuffled = false;
                            MediaPlayer.Play(ml.Songs, i);
                            MediaPlayer.IsShuffled = bs;
                            GoToPlayer();
                        }
                    }
                    else if (o is Artist)
                    {
                        Artist a = o as Artist;
                        List<Artist> ls = ml.Artists.ToList();
                        int i = ls.IndexOf(a);
                        NavigationService.Navigate(new Uri("/ArtistPage.xaml?artist=" + i.ToString(), UriKind.Relative));
                    }
                    else if (o is AlbumCustom)
                    {
                        Album a = (o as AlbumCustom).InnerAlbum;
                        List<Album> ls = ml.Albums.ToList();
                        int i = ls.IndexOf(a);
                        NavigationService.Navigate(new Uri("/ArtistPage.xaml?album=" + i.ToString(), UriKind.Relative));
                    }
                    else if (o is Playlist)
                    {
                        Playlist p = o as Playlist;
                        List<Playlist> ls = ml.Playlists.ToList();
                        int i = ls.IndexOf(p);
                        NavigationService.Navigate(new Uri("/ArtistPage.xaml?playlist=" + i.ToString(), UriKind.Relative));
                    }
                    else if (o is Genre)
                    {
                        Genre g = o as Genre;
                        List<Genre> ls = ml.Genres.ToList();
                        int i = ls.IndexOf(g);
                        NavigationService.Navigate(new Uri("/ArtistPage.xaml?genre=" + i.ToString(), UriKind.Relative));
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void gbHead_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (ml.Songs.Count > 0)
            {
                MediaPlayer.IsShuffled = false;
                MediaPlayer.IsShuffled = true;
                MediaPlayer.Play(ml.Songs);
                MediaPlayer.IsShuffled = true;
                GoToPlayer();
            }
        }

        private void btnClear0_Click(object sender, RoutedEventArgs e)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if ((sender as FrameworkElement).Name == "btnClear0")
                {
                    if (isf.DirectoryExists("\\bg"))
                    {
                        string[] s = isf.GetFileNames(@"\bg\*.*");
                        foreach(string st in s)
                        {
                            isf.DeleteFile(@"\bg\"+st);
                        }
                    }
                    IsolatedStorageSettings.ApplicationSettings["bgsize"] = (long)0;
                    IsolatedStorageSettings.ApplicationSettings.Save();
                    tbBgsize.Text = string.Format("{0} MB", 0.0);
                }
                else
                {
                    if (isf.DirectoryExists("\\lc"))
                    {
                        string[] s = isf.GetFileNames(@"\lc\*.*");
                        foreach (string st in s)
                        {
                            isf.DeleteFile(@"\lc\" + st);
                        }
                    }
                    IsolatedStorageSettings.ApplicationSettings["lcsize"] = (long)0;
                    IsolatedStorageSettings.ApplicationSettings.Save();
                    tbBgsize.Text = string.Format("{0} MB", 0.0);
                }
            }
        }

        private void hbReport_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask ect = new EmailComposeTask();
            ect.Subject = Paid.IsPaid ? "[앱]realLyrics+ contact" : "[앱]realLyrics+ contact";
            ect.To = "yygworld@outlook.com";
            string ver = Assembly.GetExecutingAssembly().FullName.ToString();
            ect.Body = System.Environment.NewLine + System.Environment.NewLine + string.Format("OS {0}" + System.Environment.NewLine + "App {1}", System.Environment.OSVersion.Version, ver);
            ect.Show();
        }

        private void llsSong_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (llsSong.SelectedItem != null && !fired)
            {
                fired = true;
                object o = llsSong.SelectedItem;
                if (o is Song)
                {
                    Song s = o as Song;
                    List<Song> ls = ml.Songs.ToList();
                    int i = ls.IndexOf(s);
                    bool bs = MediaPlayer.IsShuffled;
                    MediaPlayer.IsShuffled = false;
                    MediaPlayer.Play(ml.Songs);
                    MediaPlayer.Queue.ActiveSongIndex = i;
                    MediaPlayer.IsShuffled = bs;
                    GoToPlayer();
                }
                else if (o is Artist)
                {
                    Artist a = o as Artist;
                    List<Artist> ls = ml.Artists.ToList();
                    int i = ls.IndexOf(a);
                    NavigationService.Navigate(new Uri("/ArtistPage.xaml?artist=" + i.ToString(), UriKind.Relative));
                }
                else if (o is AlbumCustom)
                {
                    Album a = (o as AlbumCustom).InnerAlbum;
                    List<Album> ls = ml.Albums.ToList();
                    int i = ls.IndexOf(a);
                    NavigationService.Navigate(new Uri("/ArtistPage.xaml?album=" + i.ToString(), UriKind.Relative));
                }
                else if (o is Playlist)
                {
                    Playlist p = o as Playlist;
                    List<Playlist> ls = ml.Playlists.ToList();
                    int i = ls.IndexOf(p);
                    NavigationService.Navigate(new Uri("/ArtistPage.xaml?playlist=" + i.ToString(), UriKind.Relative));
                }
                else if (o is Genre)
                {
                    Genre g = o as Genre;
                    List<Genre> ls = ml.Genres.ToList();
                    int i = ls.IndexOf(g);
                    NavigationService.Navigate(new Uri("/ArtistPage.xaml?genre=" + i.ToString(), UriKind.Relative));
                }
            }
        }

        private void appNow_Click(object sender, EventArgs e)
        {
            GoToPlayer();
        }

        private void hbError_Click(object sender, RoutedEventArgs e)
        {
            string m = string.Empty;
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf != null)
                {
                    if (isf.FileExists("exception.txt"))
                    {
                        using (IsolatedStorageFileStream isfs = new IsolatedStorageFileStream("exception.txt", System.IO.FileMode.Open,
                                 isf))
                        {
                            if (isfs.CanRead)
                            {
                                using (StreamReader sw = new StreamReader(isfs))
                                {
                                    m = sw.ReadToEnd();

                                    if (m.Length > 0)
                                    {
                                        EmailComposeTask emt = new EmailComposeTask();
                                        emt.Subject = Paid.IsPaid ? "[앱]realLyrics+ error reporting!" : "[앱]realLyrics error reporting!";
                                        emt.To = "yygworld@outlook.com";
                                        emt.Body = Res.LocalString.reporting + System.Environment.NewLine + System.Environment.NewLine + m;
                                        emt.Show();
                                    }
                                }
                            }
                        }
                        isf.DeleteFile("exception.txt");
                    }
                }
            }
        }

        private void lstQuick_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = lstQuick.SelectedIndex + 1;
            if (i < pvSong.Items.Count && i > 0)
            {
                pvSong.SelectedIndex = i;
            }
            lstQuick.SelectedIndex = -1;
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (pvSong.SelectedIndex != 0)
            {
                e.Cancel = true;
                pvSong.SelectedIndex = 0;
            }
        }

        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int cnt = VisualTreeHelper.GetChildrenCount(sender as DependencyObject);
            for (int i = 0; i < cnt; i++)
            {
                DependencyObject dO = VisualTreeHelper.GetChild(sender as DependencyObject, i);
                if (dO != null && dO is Button && (dO as Button).Name == "btnPlay")
                {
                    btnPlay_Click(dO, null);
                }
            }
        }

        private void btnSearchLib_Click(object sender, RoutedEventArgs e)
        {
            btnSearchLib.IsEnabled = false;
            LocalLyrics.Initialize(ml,false,true);
            //LocalLyrics.BuildMusicTable();
        }

        bool loaded = false;
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if(loaded)
            {
                return;
            }
            loaded = true;

            int cnt = 0;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("cnt"))
            {
                cnt = (int)IsolatedStorageSettings.ApplicationSettings["cnt"];
            }
            string lastver = "";
            if (IsolatedStorageSettings.ApplicationSettings.Contains("lastver"))
            {
                lastver = (string)IsolatedStorageSettings.ApplicationSettings["lastver"];
            }
            bool rate = false;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("rate"))
            {
                rate = (bool)IsolatedStorageSettings.ApplicationSettings["rate"];
            }
            cnt++;
            IsolatedStorageSettings.ApplicationSettings["cnt"] = cnt;
            var assembly = System.Reflection.Assembly.GetExecutingAssembly().FullName;
            string ver = assembly.Split('=')[1].Split(',')[0];
            IsolatedStorageSettings.ApplicationSettings["lastver"] = ver;

            if (cnt >= Int32.MaxValue)
            {
                cnt = 6;
                IsolatedStorageSettings.ApplicationSettings["cnt"] = cnt;
            }
            IsolatedStorageSettings.ApplicationSettings.Save();

#if AD
            if(cnt % 5 != 0)
#else
            if (cnt % 5 != 0 && lastver == ver) //구매자가 평가했으면 앞으로 이 창을 보지 않아도 됨
#endif
            {
                return;
            }
#if AD
#else
            if (rate && lastver == ver)
            {
                return;
            }
#endif

            CustomMessageBox cmb = new CustomMessageBox();
            cmb.Caption = Res.LocalString.msg_cap;

            cmb.Message = Res.LocalString.msg_thank;
            if (!rate)
            {
                cmb.Message += System.Environment.NewLine + Res.LocalString.msg_ratecap;
            }
            if (lastver != ver)
            {
                cmb.Message += System.Environment.NewLine + System.Environment.NewLine + string.Format(Res.LocalString.msg_ccap, ver, Res.LocalString.msg_changelog);
            }
            cmb.IsLeftButtonEnabled = true;
            cmb.IsRightButtonEnabled = true;
            cmb.RightButtonContent = Res.LocalString.msg_later;
#if AD
            cmb.Message += System.Environment.NewLine + System.Environment.NewLine + Res.LocalString.msg_pay;
            cmb.LeftButtonContent = Res.LocalString.msg_buy;
            if (!rate)
            {
                HyperlinkButton hb = new HyperlinkButton()
                {
                    Content = Res.LocalString.msg_5rate,
                    Margin = new Thickness(0, 28, 0, 8),
                    HorizontalAlignment = HorizontalAlignment.Left
                };
                hb.Click += (a, b) =>
                {
                    rate = true;
                    IsolatedStorageSettings.ApplicationSettings["rate"] = true;
                    IsolatedStorageSettings.ApplicationSettings.Save();
                    MarketplaceReviewTask mrt = new MarketplaceReviewTask();
                    mrt.Show();
                };
                cmb.Content = hb;
            }
            cmb.Dismissed += (a, b) =>
            {
                switch(b.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        MarketplaceDetailTask mdt = new MarketplaceDetailTask();
                        mdt.ContentIdentifier = "df3d33af-1007-462f-8fa8-c30443bdb36b";
                        mdt.Show();
                        break;
                }
            };
            cmb.Show();
#else
            cmb.Message += System.Environment.NewLine + System.Environment.NewLine + Res.LocalString.msg_thankpay;
            if (rate)
            {
                cmb.IsRightButtonEnabled = false;
                cmb.RightButtonContent = null;
                cmb.LeftButtonContent = Res.LocalString.msg_close;
                cmb.IsLeftButtonEnabled = true;
            }
            else
            {
                cmb.LeftButtonContent = Res.LocalString.msg_rate;
                cmb.Dismissed += (a, b) =>
                {
                    switch (b.Result)
                    {
                        case CustomMessageBoxResult.LeftButton:
                            rate = true;
                            IsolatedStorageSettings.ApplicationSettings["rate"] = true;
                            IsolatedStorageSettings.ApplicationSettings.Save();
                            MarketplaceReviewTask mdt = new MarketplaceReviewTask();
                            mdt.Show();
                            break;
                    }
                };
            }
            cmb.Show();
#endif
        }
    }
}