﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bnReal
{
    public static class Paid
    {
#if AD
        public static bool IsPaid = false;
#else
        public static bool IsPaid = true;
#endif
    }
}
