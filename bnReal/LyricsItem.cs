﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bnReal
{
    public static class StoredMem
    {
        public static List<SongItem> mq = new List<SongItem>();
    }

    public class LyricsItem
    {
        public string Context { get; set; }
        public TimeSpan Sync { get; set; }
        public string SingleLine
        {
            get
            {
                int i = Context.IndexOfAny(new char[] {'\n', '\r'});
                if (i > 0)
                {
                    return Context.Substring(0, i);
                }
                else
                {
                    return Context;
                }
            }
        }

        public string SyncText
        {
            get
            {
                return Sync.ToString(@"h\:mm\:ss\.ff");
            }
        }

        public LyricsItem(TimeSpan a, string b)
        {
            Sync = a;
            Context = b;
        }

        public LyricsItem()
        {
        }
    }
}
