﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Microsoft.Phone.Controls;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml;
using System.Text;
using Microsoft.Phone;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Shell;
using Korean.Text.Encoding;
using Microsoft.Phone.Tasks;
using Windows.Storage;
using System.Threading.Tasks;
using Microsoft.AdMediator.Core.Models;


namespace bnReal
{
    public partial class NowPlaying : PhoneApplicationPage
    {
        void AdMediator_Bottom_AdMediatorError(object sender, Microsoft.AdMediator.Core.Events.AdMediatorFailedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AdMediatorError:" + e.Error + " " + e.ErrorCode);
            //AdMediator_315E1B.Disable();
            //AdMediator_315E1B.Visibility = System.Windows.Visibility.Collapsed;
            // if (e.ErrorCode == AdMediatorErrorCode.NoAdAvailable)
            // AdMediator will not show an ad for this mediation cycle
        }

        // 생성자
        public NowPlaying()
        {
            InitializeComponent();
            AdMediator_315E1B.AdMediatorFilled += AdMediator_315E1B_AdMediatorFilled;
            AdMediator_315E1B.AdMediatorError += AdMediator_Bottom_AdMediatorError;
            AdMediator_315E1B.AdSdkError += AdMediator_315E1B_AdSdkError;
            AdMediator_315E1B.AdSdkEvent += AdMediator_315E1B_AdSdkEvent;

            AdMediator_315E1B.AdSdkOptionalParameters[AdSdkNames.GoogleAdMob]["Parameters"] = new GoogleAds.AdRequest { Keywords = new[] { "lyrics", "music", "artist", "free" } };
#if AD
#else
            this.AdMediator_315E1B.Disable();
            this.AdMediator_315E1B.Visibility = System.Windows.Visibility.Collapsed;
            this.AdPanel.Visibility = System.Windows.Visibility.Collapsed;
#endif

            if (ranstr.Length == 0)
            {
                Random ran = new Random();
                StringBuilder ranbud = new StringBuilder();
                for (int i = 0; i < 256; i++)
                {
                    int chr = ran.Next(36);
                    if (chr < 10)
                    {
                        chr += 48;
                    }
                    else
                    {
                        chr += 87;
                    }
                    ranbud.Append(Convert.ToChar(chr));
                }
                ranstr = ranbud.ToString();
            }
            MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;
            MediaPlayer.ActiveSongChanged += MediaPlayer_ActiveSongChanged;

            this.ApplicationBar.ForegroundColor = Color.FromArgb(255,253,253,253);
            this.ApplicationBar.BackgroundColor = Color.FromArgb(255, 31, 31, 31);

            this.Language = System.Windows.Markup.XmlLanguage.GetLanguage(System.Globalization.CultureInfo.CurrentUICulture.Name);

            if (this.ApplicationBar.MenuItems.Count >= 1)
            {
                (ApplicationBar.MenuItems[0] as ApplicationBarMenuItem).Text = Res.LocalString.google;
            }
            if (!Paid.IsPaid && this.ApplicationBar.MenuItems.Count == 1)
            {
                ApplicationBarMenuItem abm = new ApplicationBarMenuItem(Res.LocalString.noad);
                abm.Click += MenuItemAd_Click;
                this.ApplicationBar.MenuItems.Add(abm);
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("view"))
            {
                int iv = (int)IsolatedStorageSettings.ApplicationSettings["view"];
                switch (iv)
                {
                    case 0: lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics0"] as DataTemplate;
                        break;
                    case 1: lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics1"] as DataTemplate;
                        break;
                    case 2: lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics2"] as DataTemplate;
                        break;
                    case 3: lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics5"] as DataTemplate;
                        break;
                    case 4: lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics10"] as DataTemplate;
                        break;
                }
            }
        }

        void AdMediator_315E1B_AdSdkEvent(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AdSdk event {0} by {1}", e.EventName, e.Name);
        }

        void AdMediator_315E1B_AdSdkError(object sender, Microsoft.AdMediator.Core.Events.AdFailedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AdSdkError by {0} ErrorCode: {1} ErrorDescription: {2} Error: {3}", e.Name, e.ErrorCode, e.ErrorDescription, e.Error);
        }

        void AdMediator_315E1B_AdMediatorFilled(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ADFilled: "+e.EventName+","+e.Name);
        }

        
        bool fs = false;
        int bg = 0;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if(!LocalLyrics.Initialized)
            {
                LocalLyrics.Initialize(new MediaLibrary());
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("bg") && IsolatedStorageSettings.ApplicationSettings["bg"] is int)
            {
                bg = (int)IsolatedStorageSettings.ApplicationSettings["bg"];
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("live"))
            {
                live = IsolatedStorageSettings.ApplicationSettings["live"] as bool? == true;
            }

            if (IsolatedStorageSettings.ApplicationSettings.Contains("full"))
            {
                fs = IsolatedStorageSettings.ApplicationSettings["full"] as bool? == true;
            }

            bool jap = System.Globalization.CultureInfo.CurrentCulture.Name == "ja-JP";
            if (IsolatedStorageSettings.ApplicationSettings.Contains("jap"))
            {
                jap = IsolatedStorageSettings.ApplicationSettings["jap"] as bool? == true;
            }
            Communication.aserver = jap ? 1 : 0;

            if (CheckPlayButton())
            {
                dtTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            }
            else
            {
                dtTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            }
            dtTimer.Tick += dtTimer_Tick;
            dtTimer.Start();
            FillMediaInfo();
            HighlightRealtime();
            if (!dtTimer.IsEnabled)
            {
                dtTimer.Start();
            }

            if (!e.IsNavigationInitiator && State.ContainsKey("fs") && State.ContainsKey("lyrics"))
            {
                bool lyrics = (bool)PhoneApplicationService.Current.State["lyrics"];
                if(!lyrics)
                {
                    return;
                }
                fullscreen = (bool)PhoneApplicationService.Current.State["fs"];
                if (ocLyrics.Count == 0)
                {
                    GetLyrics();
                    if (fullscreen)
                    {
                        bdLyrics.Visibility = System.Windows.Visibility.Visible;
                        LyricsPanel.Visibility = System.Windows.Visibility.Visible;
                    }
                }
                ToggleFullScreen(fullscreen);
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (dtTimer.IsEnabled)
            {
                dtTimer.Stop();
            }

            PhoneApplicationService.Current.State["fs"] = fullscreen;
            base.OnNavigatedFrom(e);
        }

        private void StartTransition(RotateTransitionMode rtm)
        {
            RotateTransition rotatetransition = new RotateTransition();
            rotatetransition.Mode = rtm;

            PhoneApplicationPage phoneApplicationPage =
            (PhoneApplicationPage)(((PhoneApplicationFrame)Application.Current.RootVisual)).Content;

            ITransition transition = rotatetransition.GetTransition(phoneApplicationPage);
            transition.Completed += delegate
            {
                transition.Stop();
            };
            transition.Begin();
        }

        #region 기본
        private void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            int cint = MediaPlayer.Queue.ActiveSongIndex;
            int aint = MediaPlayer.Queue.Count;
            if (cint == -1 && MediaPlayer.State == MediaState.Stopped)
            {
                MediaPlayer.Resume();
                MediaPlayer.Pause();
                cint = MediaPlayer.Queue.ActiveSongIndex;
            }
            if (aint > cint - 1 && cint - 1 >= 0)
            {
                MediaPlayer.Queue.ActiveSongIndex = cint - 1;
            }
            else if (aint > 0)
            {
                MediaPlayer.Queue.ActiveSongIndex = aint - 1;
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (dtTimer.IsEnabled)
            {
                dtTimer.Stop();
            }

            if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Pause();
                dtTimer.Interval = new TimeSpan(0, 0, 0, 2, 0);
            }
            else
            {
                MediaPlayer.Resume();
                dtTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            }
            dtTimer.Start();
            CheckPlayButton();
        }

        private void btnRight_Click(object sender, RoutedEventArgs e)
        {
            int cint = MediaPlayer.Queue.ActiveSongIndex;
            int aint = MediaPlayer.Queue.Count;
            if (cint == -1 && MediaPlayer.State == MediaState.Stopped)
            {
                MediaPlayer.Resume();
                MediaPlayer.Pause();
                cint = MediaPlayer.Queue.ActiveSongIndex;
            }
            if (aint > cint + 1 && cint + 1 >= 0)
            {
                MediaPlayer.Queue.ActiveSongIndex = cint + 1;
            }
            else if (aint > 0)
            {
                MediaPlayer.Queue.ActiveSongIndex = 0;
            }
        }

        private void btnLyrics_Click(object sender, RoutedEventArgs e)
        {
            PhoneApplicationService.Current.State["lyrics"] = LyricsPanel.Visibility == System.Windows.Visibility.Visible;
            if (gdSearch.Visibility == System.Windows.Visibility.Visible)
            {
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                bdSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                LyricsPanel.Visibility = LyricsPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed :
                    System.Windows.Visibility.Visible;
                bdLyrics.Visibility = LyricsPanel.Visibility;
                if (fs && ocLyrics.Count > 0 && LyricsPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    fullscreen = true;
                    ToggleFullScreen(true);
                }
                bool? idlesuppress = false;
                if (IsolatedStorageSettings.ApplicationSettings.Contains("idle"))
                {
                    idlesuppress = IsolatedStorageSettings.ApplicationSettings["idle"] as bool?;
                }
                else
                {
                    MessageBoxResult mbr = MessageBox.Show(Res.LocalString.noidle_1 + Environment.NewLine + Res.LocalString.noidle_2
                        + Environment.NewLine + Environment.NewLine + Res.LocalString.noidle_3,
                        Res.LocalString.noidle, MessageBoxButton.OKCancel);
                    idlesuppress = mbr == MessageBoxResult.OK ? true : mbr == MessageBoxResult.Cancel ? false : (bool?)null;
                    if (idlesuppress != null)
                    {
                        IsolatedStorageSettings.ApplicationSettings["idle"] = idlesuppress;
                        IsolatedStorageSettings.ApplicationSettings.Save();
                    }
                }
                if (idlesuppress == true)
                {
                    PhoneApplicationService.Current.UserIdleDetectionMode = LyricsPanel.Visibility == System.Windows.Visibility.Visible ? IdleDetectionMode.Disabled : IdleDetectionMode.Enabled;
                }
                if (LyricsPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    GetLyrics();

                }
            }
        }

        private void rbShuffle_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MediaPlayer.IsShuffled = !MediaPlayer.IsShuffled;
            lastsong = null;
            FillMediaInfo();
        }

        private void rbRepeat_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MediaPlayer.IsRepeating = !MediaPlayer.IsRepeating;
            FillMediaInfo();
        }

        private void btnLeft_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ShowError(Res.LocalString.noskip);
        }

        private void spNext_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            WriteableBitmap wb = new WriteableBitmap(Convert.ToInt32(this.ActualWidth), Convert.ToInt32(this.ActualHeight));
            wb.Render(LayoutRoot, null);
            wb.Invalidate();
            Playerlist.wb = wb;
            NavigationService.Navigate(new Uri("/Playerlist.xaml", UriKind.Relative));
        }

        private void ApplicationBar_StateChanged(object sender, Microsoft.Phone.Shell.ApplicationBarStateChangedEventArgs e)
        {
            this.ApplicationBar.Opacity = e.IsMenuVisible ? 0.999 : 0;
        }

        private void ShowError(string msg)
        {
            tbError.Text = msg;
            (this.Resources["storyError"] as Storyboard).Begin();
        }
        private void ShowInfo(string msg)
        {
            tbError.Text = msg;
            (this.Resources["storyInfo"] as Storyboard).Begin();
        }
        private void HideInfo()
        {
            Storyboard sb = (this.Resources["storyInfo"] as Storyboard);
            if (sb.GetCurrentState() == ClockState.Active)
            {
                TimeSpan ts = sb.GetCurrentTime();
                if (ts.Seconds < 5)
                {
                    sb.Seek(new TimeSpan(0, 0, 5));
                }
            }
        }

        ProgressIndicator pi = new ProgressIndicator();
        List<string> gmsg = new List<string>();
        private void ShowGauge(bool visible, string msg = "")
        {
            pi.Text = msg;
            if (visible)
            {
                if (!gmsg.Contains(msg))
                {
                    gmsg.Add(msg);
                }
            }
            else
            {
                if (msg == "")
                {
                    gmsg.Clear();
                }
                else if (gmsg.Contains(msg))
                {
                    gmsg.Remove(msg);
                    if (gmsg.Count > 0)
                    {
                        pi.Text = gmsg[0];
                    }
                }
            }
            pi.IsVisible = (gmsg.Count > 0);
            pi.IsIndeterminate = (gmsg.Count > 0);

            SystemTray.ProgressIndicator = pi;

            SystemTray.ForegroundColor = Color.FromArgb(255, 250, 250, 250);
            SystemTray.IsVisible = pi.IsVisible || ApplicationBar.IsVisible;
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!ApplicationBar.IsVisible)
            {
                e.Cancel = true;
                fullscreen = false;
                ToggleFullScreen(false);
            }
            else if (gdSearch.Visibility == System.Windows.Visibility.Visible)
            {
                e.Cancel = true;
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                bdSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (LyricsPanel.Visibility == System.Windows.Visibility.Visible)
            {
                e.Cancel = true;
                LyricsPanel.Visibility = System.Windows.Visibility.Collapsed;
                PhoneApplicationService.Current.State["lyrics"] = false;
                bdLyrics.Visibility = LyricsPanel.Visibility;
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;
            }
        }

        private void ListBoxItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/ArtistPage.xaml?artist_txt=" + HttpUtility.UrlEncode(rartist), UriKind.Relative));
        }

        #endregion

        #region 기본 세부
        private void pvAlbum_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pvAlbum.SelectedIndex == 0)
            {
                btnLeft_Click(sender, null);
            }
            else if (pvAlbum.SelectedIndex == 2)
            {
                btnRight_Click(sender, null);
            }

            this.Dispatcher.BeginInvoke(() =>
            {
                if (pvAlbum.SelectedIndex != 1)
                {
                    pvAlbum.SelectionChanged -= pvAlbum_SelectionChanged;
                    pvAlbum.Visibility = System.Windows.Visibility.Collapsed;
                    if (pvAlbum.SelectedIndex == 0)
                    {
                        pvAlbum.SelectedIndex = 2;
                        pvAlbum.ApplyTemplate();
                        pvAlbum.SelectedIndex = 1;
                        pvAlbum.ApplyTemplate();
                    }
                    else
                    {
                        pvAlbum.SelectedIndex = 0;
                        pvAlbum.ApplyTemplate();
                        pvAlbum.SelectedIndex = 1;
                        pvAlbum.ApplyTemplate();
                    }
                    pvAlbum.Visibility = System.Windows.Visibility.Visible;
                    pvAlbum.SelectionChanged += pvAlbum_SelectionChanged;
                }
            });
        }
        #endregion

        #region 미디어 갱신
        DispatcherTimer dtTimer = new DispatcherTimer();
        int freq = 0;
        void dtTimer_Tick(object sender, EventArgs e)
        {
            freq++;
            bool playing = CheckPlayButton();
            if (freq % 10 == 1)
            {
                freq = 0;
                FillMediaInfo();
            }
            if (playing && (!string.IsNullOrEmpty(lrc) || ocLyrics.Count > 1 && ocLyrics[0].Sync >= TimeSpan.Zero) && LyricsPanel.Visibility == System.Windows.Visibility.Visible)
            {
                HighlightRealtime();
            }
        }
        #endregion

        #region 미디어
        void MediaPlayer_MediaStateChanged(object sender, EventArgs e)
        {
            CheckPlayButton();
        }

        void MediaPlayer_ActiveSongChanged(object sender, EventArgs e)
        {
            FillMediaInfo();
        }

        private bool CheckPlayButton()
        {
            bool playing = MediaPlayer.State == MediaState.Playing;
            if (recPlay.Tag as int? != 1 && playing)
            {
                recPlay.OpacityMask = new ImageBrush() { ImageSource = new BitmapImage(new Uri("/Images/transport.pause.png", UriKind.Relative)) };
                recPlay.Tag = 1;
            }
            else if (recPlay.Tag as int? != 0 && !playing)
            {
                recPlay.OpacityMask = new ImageBrush() { ImageSource = new BitmapImage(new Uri("/Images/transport.play.png", UriKind.Relative)) };
                recPlay.Tag = 0;
            }
            return playing;
        }

        Song lastsong = null;
        private void FillMediaInfo()
        {
            try
            {
                if (MediaPlayer.Queue != null)
                {
                    if (MediaPlayer.Queue.ActiveSong == null && MediaPlayer.State != MediaState.Playing)
                    {
                        MediaPlayer.Resume();
                        MediaPlayer.Pause();
                    }
                    if (MediaPlayer.Queue.ActiveSong != null)
                    {
                        Song sn = MediaPlayer.Queue.ActiveSong;
                        if (sn != lastsong)
                        {
                            SongChanged(MediaPlayer.Queue.ActiveSongIndex, sn);
                        }
                        tbDuration.Text = MediaPlayer.PlayPosition.ToString("m':'ss");
                        tbRDuration.Text = (sn.Duration - MediaPlayer.PlayPosition).ToString("'-'m':'ss");
                        pbDuration.Maximum = sn.Duration.TotalSeconds;
                        pbDuration.Value = MediaPlayer.PlayPosition.TotalSeconds;

                        rbShuffle.Opacity = MediaPlayer.IsShuffled ? 1 : 0.5;
                        rbRepeat.Opacity = MediaPlayer.IsRepeating ? 1 : 0.5;
                        return;
                    }
                }

                ShellTile st = ShellTile.ActiveTiles.First();
                st.Update(new StandardTileData()
                {
                    BackBackgroundImage = new Uri("/blank.img",
                        UriKind.Relative),
                    Count = 0
                });

                if (NavigationService.CanGoBack)
                {
                    NavigationService.GoBack();
                }
            }
            catch
            {

            }
        }

        private void SongChanged(int idx, Song sn)
        {
            lrc = "";
            lpage = 1;
            ocLyrics.Clear();
            bdThumb.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(140, 80, 80, 80));
            imgThumb.Source = null;
            LayoutRoot.Tag = null;
            if (sn.Album != null)
            {
                try
                {
                    using (Stream s = sn.Album.GetAlbumArt())
                    {
                        if (s == null)
                        {
                            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                            {
                                using (IsolatedStorageFileStream isfs = isf.OpenFile(@"\Shared\ShellContent\album.img", FileMode.Create))
                                {
                                    isfs.Flush();
                                }
                            }
                            ShellTile st = ShellTile.ActiveTiles.First();
                            st.Update(new StandardTileData()
                            {
                                BackBackgroundImage = new Uri("isostore:/Shared/ShellContent/album.img",
                                    UriKind.Absolute),
                                Count = 0
                            });
                        }
                        else
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                s.CopyTo(ms);
                                ms.Position = 0;
                                BitmapImage bi = new BitmapImage() { CreateOptions = BitmapCreateOptions.BackgroundCreation };
                                bi.SetSource(ms);
                                imgThumb.Source = bi;
                                bdThumb.Background = null;
                                ms.Position = 0;
                                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                {
                                    using (IsolatedStorageFileStream isfs = isf.OpenFile(@"\Shared\ShellContent\album.img", FileMode.Create))
                                    {
                                        ms.CopyTo(isfs);
                                        isfs.Flush();
                                    }
                                }
                                ShellTile st = ShellTile.ActiveTiles.First();
                                st.Update(new StandardTileData()
                                {
                                    BackBackgroundImage = new Uri("isostore:/Shared/ShellContent/album.img",
                                        UriKind.Absolute),
                                    Count = 0
                                });
                            }
                        }
                    }

                    if (idx > 0)
                    {
                        Song before = MediaPlayer.Queue[idx - 1];
                        if (before != null)
                        {
                            using (Stream sb = before.Album.GetAlbumArt())
                            {
                                if (sb != null)
                                {
                                    BitmapImage bbi = new BitmapImage() { CreateOptions = BitmapCreateOptions.BackgroundCreation };
                                    bbi.SetSource(sb);
                                }
                            }
                        }
                    }
                    if (idx < MediaPlayer.Queue.Count - 1)
                    {
                        Song next = MediaPlayer.Queue[idx + 1];
                        if (next != null)
                        {
                            using (Stream sb = next.Album.GetAlbumArt())
                            {
                                if (sb != null)
                                {
                                    BitmapImage bbi = new BitmapImage() { CreateOptions = BitmapCreateOptions.BackgroundCreation };
                                    bbi.SetSource(sb);
                                }
                            }
                        }
                    }

                }
                catch
                {

                }
            }

            if ((sn.Artist != null && tbArtist.Text != sn.Artist.Name) || (sn.Album != null && tbAlbum.Text != sn.Album.Name))
            {
                (this.Resources["storyAlbum"] as Storyboard).Begin();
            }
            tbArtist.Text = sn.Artist == null ? string.Empty : sn.Artist.Name;
            tbAlbum.Text = sn.Album == null ? string.Empty : sn.Album.Name;
            rtitle = sn.Name;
            rartist = tbArtist.Text;
            SimplizeSong();

            int ci = MediaPlayer.Queue.ActiveSongIndex;
            if (ci == -1)
            {
                MediaPlayer.Resume();
                MediaPlayer.Pause();
                ci = MediaPlayer.Queue.ActiveSongIndex;
            }
            if (tbTitle.Text != sn.Name || tbNext.Text != (ci + 1 < MediaPlayer.Queue.Count ? MediaPlayer.Queue[ci + 1].Name : ""))
            {
                (this.Resources["storyTitle"] as Storyboard).Begin();
                tbTitle.Text = sn.Name;
            }
            tbNext.Text = ci + 1 < MediaPlayer.Queue.Count ? MediaPlayer.Queue[ci + 1].Name : "";
            tbNextMore.Text = ci + 2 < MediaPlayer.Queue.Count ? MediaPlayer.Queue[ci + 2].Name : "";
            lastsong = sn;
            LoadBackgroundImage();
            GetLyricsCount();
            if (LyricsPanel.Visibility == System.Windows.Visibility.Visible)
            {
                GetLyrics();
            }
        }
        #endregion

        #region 가사
        string rtitle = "", rartist = "", ctitle = "", cartist = "";
        int cntlyrics = 0;
        string ranstr = "";
        ObservableCollection<LyricsItem> ocLyrics = new ObservableCollection<LyricsItem>();
        string lrc = "";
        string SimplizeText(string v)
        {
            v = v.Replace("<", " ");
            v = v.Replace(">", " ");
            return HttpUtility.UrlEncode(v);
        }

        void SimplizeSong()
        {
            cartist = rartist;
            ctitle = rtitle;
            string[] noneeds = { "(inst", "(duet", "(feat", "(off v", 
                                           "-off v", "-inst", "-duet", "-feat", "inst.", "duet.", "feat." };
            foreach (string filt in noneeds)
            {
                int n = ctitle.IndexOf(filt, StringComparison.CurrentCultureIgnoreCase);
                if (n >= 0)
                {
                    ctitle = ctitle.Substring(0, n);
                }
            }
            ctitle = ctitle.Replace("..", "");
            string[] art_noneeds = { "(", "[", "<", "≪", "《", "（" };
            foreach (string filt in art_noneeds)
            {
                int n = cartist.IndexOf(filt);
                if (n >= 0)
                {
                    cartist = cartist.Substring(0, n).Trim();
                }
            }

            if (Communication.aserver == 1)
            {
                ctitle = ctitle.Trim();
                cartist =  cartist.Trim();
            }
            else
            {
                ctitle = EUCMatch(ctitle).Trim();
                cartist = EUCMatch(cartist).Trim();
            }

            ptbTitle.Text = ctitle;
            ptbArtist.Text = cartist;
            ocLR.Clear();

            ctitle = HttpUtility.UrlEncode(ctitle);
            cartist = HttpUtility.UrlEncode(cartist);
        }

        private string EUCMatch(string st)
        {
            EuckrEncoding ee = new EuckrEncoding();
            ee.FallbackChar = '?';
            byte[] bm = ee.GetBytes(st);
            EuckrDecoder ed = new EuckrDecoder();
            ed.FallbackChar = '?';
            char[] cs = new char[bm.Length];
            ed.GetChars(bm, 0, bm.Length, cs, 0, true);
            string euctitle = new string(cs);
            euctitle = euctitle.Replace("?", "").Replace("\0", "");

            StringBuilder sb = new StringBuilder();
            int icarrot = 0, isc = 0;
            bool left = false;
            while (icarrot < st.Length)
            {
                if (isc < euctitle.Length)
                {
                    if (st[icarrot] == euctitle[isc])
                    {
                        sb.Append(st[icarrot]);
                        icarrot++;
                        isc++;
                    }
                    else
                    {
                        if (left)
                        {
                            isc++;
                        }
                        else
                        {
                            icarrot++;
                        }
                        left = !left;
                    }
                }
                else
                {
                    break;
                }
            }

            return sb.ToString();
        }

        void GetLyricsCount()
        {
            ShowGauge(true, "  ");
            tbCount.Text = "...";

            if (MediaPlayer.Queue.ActiveSong != null)
            {
                if (LocalLyrics.HasLocalLRC(MediaPlayer.Queue.ActiveSong))
                {
                    tbCount.Text = "LRC";
                    ShowGauge(false, "  ");
                    return;
                }
            }

            lwr.Add(Communication.CommunicateALSong("/ALSongMobile/MobileSearchLyric.aspx?mode=count&title="
                        + ctitle + "&artist=" + cartist + "&key=" + ranstr.Substring(0, Math.Min(256, ranstr.Length)), (suc, res) =>
                        {
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                if (suc >= 0)
                                {
                                    using (XmlReader xr = XmlReader.Create(res))
                                    {
                                        try
                                        {
                                            if (xr.ReadToFollowing("lyricCount"))
                                            {

                                                xr.MoveToContent();
                                                string cnt = xr.ReadInnerXml();

                                                cntlyrics = Convert.ToInt32(cnt);
                                            }
                                        }
                                        catch
                                        {
                                            cntlyrics = -1;
                                            //lyricInfo.Text += string.Format("+?");
                                        }
                                        if (cntlyrics >= 0)
                                        {
                                            tbCount.Text = cntlyrics.ToString();
                                            ShellTile st = ShellTile.ActiveTiles.First();
                                            st.Update(new StandardTileData()
                                            {
                                                Count = cntlyrics
                                            });
                                        }
                                        else
                                        {
                                            tbCount.Text = "X";
                                        }
                                    }
                                }
                                else
                                {
                                    tbCount.Text = "?";
                                }
                                ShowGauge(false, "  ");
                                if (res != null)
                                {
                                    res.Dispose();
                                    res = null;
                                }
                            });
                        }));
        }

        async void GetLyrics(bool force = false, string lid = null)
        {
            if (MediaPlayer.Queue.ActiveSong == null)
            {
                return;
            }

            ChangeLyricsView(true);

            if (lrc.Length == 0 || force)
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isf.DirectoryExists(@"\lc"))
                    {
                        string lycfile = Filenaming(string.Format(@"\lc\{0}-{1}.lcf", HttpUtility.UrlDecode(ctitle), HttpUtility.UrlDecode(cartist)));
                        if (isf.FileExists(lycfile))
                        {
                            if (force)
                            {
                                using (IsolatedStorageFileStream isfs = isf.OpenFile(lycfile, FileMode.Open))
                                {
                                    long size = 0;
                                    if (IsolatedStorageSettings.ApplicationSettings.Contains("lcsize"))
                                    {
                                        size = (long)IsolatedStorageSettings.ApplicationSettings["lcsize"];
                                    }
                                    IsolatedStorageSettings.ApplicationSettings["lcsize"] = Math.Max(0, size - isfs.Length);
                                    IsolatedStorageSettings.ApplicationSettings.Save();
                                }
                                isf.DeleteFile(lycfile);
                            }
                            else
                            {
                                using (IsolatedStorageFileStream isfs = isf.OpenFile(lycfile, FileMode.Open))
                                {
                                    if (isfs.Length != 0)
                                    {
                                        RealignLyrics(isfs, true);
                                        lbLyrics.ItemsSource = ocLyrics;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                if (lid == null && MediaPlayer.Queue.ActiveSong != null)
                {
                    var lyrics = await LocalLyrics.FillLRC(MediaPlayer.Queue.ActiveSong);
                    if (lyrics != null)
                    {
                        if (ocLyrics == null)
                        {
                            ocLyrics = new ObservableCollection<LyricsItem>();
                        }
                        ocLyrics.Clear();
                        foreach (var l in lyrics)
                        {
                            ocLyrics.Add(l);
                        }
                        sitem = 0;
                        litem = -1;
                        lbLyrics.ItemsSource = ocLyrics;
                        if (lyrics.Count > 0)
                        {
                            return;
                        }
                    }
                }

                ShowGauge(true, Res.LocalString.downlyrics);
                string req = "";
                if (lid == null)
                {
                    req = "/ALSongMobile/MobileSyncLyric.aspx?title="
                                    + ctitle
                                    + "&artist=" + cartist
                                    + "&album=" + SimplizeText(MediaPlayer.Queue.ActiveSong.Album.Name)
                                    + "&playtime="
                                    + (Convert.ToInt32(MediaPlayer.Queue.ActiveSong.Duration.TotalSeconds) * 1000).ToString()
                                    + "&key="
                                    + ranstr.Substring(0, Math.Min(256, ranstr.Length));
                }
                else
                {
                    req = "/ALSongMobile/MobileGetLyric.aspx?lyricID=" + lid
                                    + "&key=" + ranstr.Substring(0, Math.Min(256, ranstr.Length));
                }
                lwr.Add(Communication.CommunicateALSong(req, (suc, res) =>
                            {
                                this.Dispatcher.BeginInvoke(async () =>
                                {
                                    if (suc >= 0)
                                    {
                                        RealignLyrics(res, false);
                                    }
                                    else
                                    {
                                        ShowError(Res.LocalString.downlyricsfail);
                                        ChangeLyricsView(await ShowMetaLyrics());
                                        lrc = "";
                                    }
                                    ShowGauge(false, Res.LocalString.downlyrics);
                                    if (res != null)
                                    {
                                        res.Dispose();
                                        res = null;
                                    }
                                });
                            }));
            }
            litem = -1;
            lbLyrics.ItemsSource = ocLyrics;
        }

        private async void RealignLyrics(Stream str, bool cachemode)
        {
            List<string> ltsl = new List<string>(); //싱크 중복 검출용 리스트    
            List<LyricsItem> lyrics = new List<LyricsItem>();
            try
            {
                ltsl.Clear();
                lyrics.Clear();
                StreamReader ms = new StreamReader(str);
                lrc = ms.ReadToEnd();
                str.Position = 0;
                using (XmlReader xr = XmlReader.Create(str))
                {
                    if (xr.ReadToFollowing("lyricID"))
                    {
                        //
                    }
                    if (xr.ReadToFollowing("title"))
                    {
                        xr.MoveToContent();
                    }
                    if (xr.ReadToFollowing("artist"))
                    {
                        xr.MoveToContent();
                    }
                    if (xr.ReadToFollowing("album"))
                    {
                        xr.MoveToContent();
                    }

                    #region 가사 추가 반복문
                    while (xr.ReadToFollowing("lyric"))
                    {
                        string times = xr["time"];
                        double timing = 0.0;
                        try
                        {
                            timing = Convert.ToDouble(times);
                        }
                        catch
                        {
                            continue;
                        }
                        if (ltsl.Count > 0)
                        {
                            if (Convert.ToDouble(ltsl[ltsl.Count - 1]) > timing)
                            {
                                continue; //싱크 순서가 뒤바뀐 가사는 무시
                            }
                        }
                        int hour = Convert.ToInt32(Math.Floor(timing / 60 / 60));
                        int min = Convert.ToInt32(Math.Floor(timing / 60) - hour * 60);
                        int sec = Convert.ToInt32(Math.Floor(timing) - min * 60 - hour * 60 * 60);
                        int msec = Convert.ToInt32(timing * 1000 - Math.Floor(timing) * 1000);
                        TimeSpan ts = new TimeSpan(0, hour, min, sec, msec);

                        xr.MoveToContent();
                        string s = HttpUtility.HtmlDecode(xr.ReadInnerXml().Replace(" / ", Environment.NewLine)).Replace("<br>", Environment.NewLine);
                        if (s.Replace(" ", "").Length == 0)
                        {
                            continue;
                        }
                        if (ltsl.Contains(times))
                        {
                            lyrics[ltsl.Count - 1].Context += Environment.NewLine + s;
                        }
                        else
                        {
                            lyrics.Add(new LyricsItem(ts, s.Trim()));
                            ltsl.Add(times);
                        }
                    }
                    #endregion

                    #region 성공 동작
                    if (lyrics.Count > 0)
                    {
                        Storyboard sb = Resources["storyError"] as Storyboard;
                        if (sb.GetCurrentState() == ClockState.Active)
                        {
                            sb.SkipToFill();
                        }
                        firstscroll = true;
                        sitem = 0;
                        litem = -1;

                        if (fs && LyricsPanel.Visibility == System.Windows.Visibility.Visible && this.ApplicationBar.IsVisible)
                        {
                            fullscreen = true;
                            ToggleFullScreen(true);
                        }

                        HighlightRealtime();

                        #region 가사 캐시 저장
                        if (!cachemode)
                        {
                            try
                            {
                                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                                {
                                    if (!isf.DirectoryExists("lc"))
                                    {
                                        isf.CreateDirectory("lc");
                                    }
                                    string lycfile = Filenaming(string.Format(@"\lc\{0}-{1}.lcf", HttpUtility.UrlDecode(ctitle), HttpUtility.UrlDecode(cartist)));
                                    long lastsize = 0, size = 0;
                                    if (isf.FileExists(lycfile))
                                    {
                                        using (IsolatedStorageFileStream isfssize = isf.OpenFile(lycfile, FileMode.Open))
                                        {
                                            lastsize = isfssize.Length;
                                        }
                                    }
                                    using (IsolatedStorageFileStream isfs = isf.OpenFile(lycfile, FileMode.Create))
                                    {
                                        using (StreamWriter sr = new StreamWriter(isfs))
                                        {
                                            //sr.Write(string.Format("{0}_", idxnum));
                                            sr.Write(lrc);
                                            sr.Flush();

                                            if (IsolatedStorageSettings.ApplicationSettings.Contains("lcsize"))
                                            {
                                                size = (long)IsolatedStorageSettings.ApplicationSettings["lcsize"];
                                            }
                                            IsolatedStorageSettings.ApplicationSettings["lcsize"] = size - lastsize + isfs.Length;
                                            IsolatedStorageSettings.ApplicationSettings.Save();
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                ShowError(Res.LocalString.cacheerror);
                            }
                        }
                        #endregion

                    }
                    #endregion 성공 동작 끝
                }

                #region 실패 동작
                if (lyrics.Count == 0)
                {
                    ShowError(Res.LocalString.nomatch);
                    ChangeLyricsView(false);
                    lrc = "";
                }
                #endregion
            }
            catch
            {
                ShowError(Res.LocalString.lyricserror);
                ShowMetaLyrics();
            }

            ocLyrics.Clear();
            foreach (LyricsItem li in lyrics)
            {
                ocLyrics.Add(li);
            }
            litem = -1;
        }

        private async Task<bool> ShowMetaLyrics()
        {
            if (MediaPlayer.Queue.ActiveSong != null)
            {
                var lyrics = await LocalLyrics.ShowMetaLyrics(MediaPlayer.Queue.ActiveSong);

                if (lyrics != null)
                {
                    if (ocLyrics == null)
                    {
                        ocLyrics = new ObservableCollection<LyricsItem>();
                    }
                    ocLyrics.Clear();
                    foreach (var l in lyrics)
                    {
                        ocLyrics.Add(l);
                    }
                    sitem = 0;
                    litem = -1;
                    lbLyrics.ItemsSource = ocLyrics;
                    return true;
                }
            }
            return false;
        }

        private void ChangeLyricsView(bool visible)
        {
            pvLyrics.SelectionChanged -= pvLyrics_SelectionChanged;

            LyricsPanel.VerticalAlignment = visible ? System.Windows.VerticalAlignment.Stretch : System.Windows.VerticalAlignment.Top;
            lbLyrics.Visibility = visible ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            tbLyrics.Visibility = visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

            if (visible && fullscreen && ApplicationBar.IsVisible)
            {
                ToggleFullScreen(true);
            }
            else if (!visible && !ApplicationBar.IsVisible)
            {
                ToggleFullScreen(false);
                return;
            }

            if (!visible && LyricsPanel.Visibility == System.Windows.Visibility.Visible)
            {
                gdSearch.Visibility = System.Windows.Visibility.Visible;
                bdSearch.Visibility = System.Windows.Visibility.Visible;
            }
            else if (visible && LyricsPanel.Visibility == System.Windows.Visibility.Visible)
            {
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                bdSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
            pvLyrics.SelectionChanged += pvLyrics_SelectionChanged;
        }

        bool firstscroll = false; int sitem = -1, litem = -1; //sitem : 반복문 시작 가사 라인, litem: 선택된 가사 라인
        private void HighlightRealtime()
        {
            try
            {
                if (MediaPlayer.Queue == null || MediaPlayer.Queue.ActiveSong == null ||
                    MediaPlayer.Queue.Count <= 0 || ocLyrics == null || ocLyrics.Count == 0)
                {
                    return;
                }

                TimeSpan cs = MediaPlayer.PlayPosition;
                bool found = false;
            retry:
                for (int i = sitem; i < ocLyrics.Count; i++)
                {
                    LyricsItem li = ocLyrics[i];
                    if (li.Sync > cs)
                    {
                        if (i > 0)
                        {
                            LyricsItem ji = ocLyrics[i - 1];
                            if (ji.Sync > cs)
                            {
                                break;
                            }
                        }

                        if (firstscroll)
                        {
                            lbLyrics.ScrollIntoView(ocLyrics[0]);
                            lbLyrics.UpdateLayout();
                            lbLyrics.SelectedIndex = -1;
                        }
                        if (litem != i - 1)
                        {
                            if (gdSearch.Visibility == System.Windows.Visibility.Collapsed)
                            {
                                lbLyrics.Focus();
                            }
                            lbLyrics.ScrollIntoView(ocLyrics[Math.Min(i + 1, ocLyrics.Count - 1)]);
                            lbLyrics.UpdateLayout();
                            lbLyrics.SelectedIndex = i - 1;
                            lbLyrics.ScrollIntoView(ocLyrics[Math.Max(0,i - 1)]);
                            lbLyrics.UpdateLayout();
                            sitem = i;
                        }
                        found = true;
                        break;
                    }
                }

                if (!found && ocLyrics.Count > 0 && ocLyrics[ocLyrics.Count - 1].Sync <= cs)
                {
                    if (litem != ocLyrics.Count - 1)
                    {
                        if (gdSearch.Visibility == System.Windows.Visibility.Collapsed)
                        {
                            lbLyrics.Focus();
                        }
                        lbLyrics.SelectedIndex = ocLyrics.Count - 1;
                    }
                }
                else if (!found)
                {
                    sitem = 0;
                    goto retry;
                }
            }
            catch
            {

            }
            firstscroll = false;
        }
        #endregion

        #region 배경
        List<HttpWebRequest> lwr = new List<HttpWebRequest>();
        private void LoadBackgroundImage()
        {
            while (lwr.Count > 0)
            {
                HttpWebRequest l = lwr[0];
                if (l != null)
                {
                    l.Abort();
                }
                lwr.RemoveAt(0);
            }
            imgThumb.Opacity = 1;
            LayoutRoot.Background = null;
            if (lastsong.Artist != null)
            {
                string artist = @"\bg\" + Filenaming(lastsong.Artist.Name) + ".img";
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isf.DirectoryExists(@"\bg"))
                    {
                        if (isf.FileExists(artist))
                        {
                            using (IsolatedStorageFileStream isfs = isf.OpenFile(artist, FileMode.Open))
                            {
                                if (isfs.Length == 0)
                                {
                                    isfs.Close();
                                    isf.DeleteFile(artist);
                                }
                                else
                                {
                                    StreamToBackground(isfs);
                                    return;
                                }
                            }
                        }
                    }
                }

                if (bg != 1)
                {
                    FailImage();
                    return;
                }

                ShowGauge(true, Res.LocalString.downimage);
                string query = lastsong.Artist.Name.Trim();
                lwr.Add(Communication.Communicate("http://www.musicbrainz.org/ws/2/artist/?query=artist:" + HttpUtility.UrlEncode(query),
                    (asuc, ares) =>
                    {
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            string aid = "";
                            try
                            {
                                
                                if (asuc >= 0)
                                {
                                    byte[] bt = Encoding.UTF8.GetBytes(ares);
                                    MemoryStream ms = new MemoryStream(bt);
                                    ms.Position = 0;
                                    XmlReader xr = XmlReader.Create(ms);
                                    while (xr.ReadToFollowing("artist"))
                                    {
                                        try
                                        {
                                            aid = xr["id"];
                                            break;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                    ms.Dispose();
                                }
                            }
                            catch
                            {
                                ShowError(Res.LocalString.lyricserrorxml);
                            }

                            //if (lastsong.Album != null)
                            //{
                            //    string[] chunk = lastsong.Album.Name.Trim().Split(' ');
                            //    if (chunk.Length > 0)
                            //    {
                            //        query += " \"" + chunk[0] +"\"";
                            //    }
                            //}
                            this.Dispatcher.BeginInvoke(() =>
                            {
                                /*if (aid.Length > 0)
                                {
                                    lwr.Add(Communication.Communicate("http://api.fanart.tv/webservice/artist/9f19b578756bcb9b2b7676bc0a0764c8/" + aid + "/XML/all/1/1",
                                        (fan, art) =>
                                        {
                                            if (fan >= 0)
                                            {
                                                this.Dispatcher.BeginInvoke(() =>
                                                {
                                                    string aurl = "";
                                                    byte[] bt = Encoding.Unicode.GetBytes(art);
                                                    MemoryStream ms = new MemoryStream(bt);
                                                    ms.Position = 0;
                                                    XmlReader xr = XmlReader.Create(ms);
                                                    while (xr.ReadToFollowing("artistbackground"))
                                                    {
                                                        try
                                                        {
                                                            aurl = xr["url"];
                                                            break;
                                                        }
                                                        catch
                                                        {
                                                        }
                                                    }
                                                    ms.Dispose();

                                                    if (aurl.Length > 0)
                                                    {
                                                        UrlToBackground(aurl);
                                                    }
                                                    else
                                                    {
                                                        GoogleMethod(query, aid);
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                GoogleMethod(query, aid);
                                            }
                                        }));
                                }
                                else
                                {*/
                                GoogleMethod(query, aid);
                                /*}*/
                            });
                        });
                    }));
            }


        }

        private void GoogleMethod(string query, string aid)
        {
            string u = "http://ajax.googleapis.com/ajax/services/search/images?q=" + HttpUtility.UrlEncode(aid.Length > 0 ? HttpUtility.UrlEncode(aid) : query) + "&v=1.0";
            lwr.Add(Communication.Communicate(u, (suc, res) =>
            {
                if (suc >= 0)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        LayoutRoot.Tag = query;
                        if (!GoogleResultToBackground(res) && aid.Length > 0)
                        {
                            u = "http://ajax.googleapis.com/ajax/services/search/images?q=" + HttpUtility.UrlEncode(query) + "&v=1.0";
                            lwr.Add(Communication.Communicate(u, (bsuc, bres) =>
                            {
                                this.Dispatcher.BeginInvoke(() =>
                                    {
                                        LayoutRoot.Tag = query;
                                        if (bsuc >= 0)
                                        {

                                            if (!GoogleResultToBackground(bres))
                                            {
                                                ShowGauge(false, Res.LocalString.downimage);
                                                if (Environment.OSVersion.Version.Major >= 8)
                                                {
                                                    GradientStopCollection gsc = new GradientStopCollection();
                                                    gsc.Add(new GradientStop() { Color = System.Windows.Media.Color.FromArgb(255, 0, 0, 0), Offset = 0 });
                                                    gsc.Add(new GradientStop() { Color = System.Windows.Media.Color.FromArgb(255, 127, 127, 127), Offset = 1 });
                                                    LinearGradientBrush lgb = new LinearGradientBrush(gsc, 90);
                                                    LayoutRoot.Background = lgb;
                                                }
                                                else
                                                {
                                                    LayoutRoot.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 20, 20, 20));
                                                }
                                            }

                                        }
                                        else
                                        {
                                            FailImage();
                                        }
                                    });
                            }));
                        }
                        else if (aid.Length == 0)
                        {
                            FailImage();
                        }
                    });
                }
                else
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        FailImage();
                    });
                }
            }));
        }

        private bool GoogleResultToBackground(string res)
        {
            try
            {
                JObject json = (JObject)JsonConvert.DeserializeObject(res);
                string url = "";
                JArray jresult = json["responseData"]["results"] as JArray;
                bool sitefirst = jresult.ToString().IndexOf("fanart.tv") >= 0;
                foreach(var j in jresult)
                {
                    int width = 0, height = 0;
                    int.TryParse(j["width"].ToString(), out width);
                    int.TryParse(j["height"].ToString(), out height);
                    string s = j["unescapedUrl"].ToString();
                    if(sitefirst && s.IndexOf("fanart.tv") >= 0)
                    {
                        url = s;
                        break;
                    }
                    else if(!sitefirst && width >= 480 && height >= 480)
                    {
                        url = s;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(url))
                {
                    return false;
                }
                else
                {
                    UrlToBackground(url);
                    return true;
                }
            }
            catch
            {
            }
            //FailImage();
            return false;
        }

        private void StreamToBackground(Stream str)
        {
            BitmapImage bi = new BitmapImage();
            bi.SetSource(str);
            ImageBrush ib = new ImageBrush()
            {
                Opacity = 0.4,
                Stretch = System.Windows.Media.Stretch.UniformToFill
            };
            ib.ImageOpened += ib_ImageOpened;
            ib.ImageFailed += ib_ImageFailed;
            ib.ImageSource = bi;
            LayoutRoot.Background = ib;
            SuccessImage(true);
        }

        private void UrlToBackground(string url)
        {
            Communication.CommunicateBinary(url, (suc, str, ab) =>
            {
                this.Dispatcher.BeginInvoke(() =>
                    {
                        if (suc >= 0)
                        {
                            ImageSource wb = null;
                            try
                            {
                                wb = PictureDecoder.DecodeJpeg(str, 1024, 1024);
                            }
                            catch
                            {
                                wb = new BitmapImage() { CreateOptions = BitmapCreateOptions.BackgroundCreation };
                                (wb as BitmapImage).UriSource = new Uri(url);
                            }
                            ImageBrush ib = new ImageBrush()
                            {
                                Opacity = 0.4,
                                Stretch = System.Windows.Media.Stretch.UniformToFill
                            };
                            ib.ImageOpened += ib_ImageOpened;
                            ib.ImageFailed += ib_ImageFailed;
                            ib.ImageSource = wb;
                            LayoutRoot.Background = ib;
                            SuccessImage();
                        }
                        else
                        {
                            FailImage();
                        }
                    });
            });
        }

        void ib_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            FailImage();
        }

        private void FailImage()
        {
            ShowGauge(false, Res.LocalString.downimage);
            if (Environment.OSVersion.Version.Major >= 8 && bg == 0)
            {
                GradientStopCollection gsc = new GradientStopCollection();
                gsc.Add(new GradientStop() { Color = System.Windows.Media.Color.FromArgb(255, 40, 40, 40), Offset = 0 });
                gsc.Add(new GradientStop() { Color = System.Windows.Media.Color.FromArgb(255, 127, 127, 127), Offset = 1 });
                LinearGradientBrush lgb = new LinearGradientBrush(gsc, 90);
                LayoutRoot.Background = lgb;
            }
            else
            {
                //LayoutRoot.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 50, 50, 50));
                LayoutRoot.Background = new SolidColorBrush(Colors.Black);
            }
        }

        void ib_ImageOpened(object sender, RoutedEventArgs e)
        {
            SuccessImage();
        }

        private string Filenaming(string filename)
        {
            return filename.Replace('*', '_').Replace('?', '_').Replace('\\', '_').Replace('/', '_').Replace('"', '_')
                    .Replace('\'', '_').Replace(':', '_').Replace('|', '_').Replace('<', '_').Replace('>', '_').Replace('%', '_');
        }

        private void SuccessImage(bool ignorecaching = false)
        {
            imgThumb.Opacity = 0.8;
            ShowGauge(false, Res.LocalString.downimage);

            if (!ignorecaching && lastsong.Artist != null)
            {
                string art = LayoutRoot.Tag == null ? lastsong.Artist.Name : LayoutRoot.Tag as string;
                string artist = @"\bg\" + Filenaming(art) + ".img";
                ImageSource ii = (LayoutRoot.Background as ImageBrush).ImageSource;
                if (ii != null)
                {
                    using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        if (!isf.DirectoryExists(@"\bg"))
                        {
                            isf.CreateDirectory(@"\bg");
                        }
                        long lastsize = 0;
                        if (isf.FileExists(artist))
                        {
                            using (IsolatedStorageFileStream isfs = isf.OpenFile(artist, FileMode.OpenOrCreate))
                            {
                                lastsize = isfs.Length;
                            }
                        }
                        using (IsolatedStorageFileStream isfs = isf.OpenFile(artist, FileMode.Create))
                        {
                            WriteableBitmap wb = null;
                            if (ii is BitmapImage)
                            {
                                return;
                                wb = new WriteableBitmap(ii as BitmapImage);
                            }
                            else if (ii is WriteableBitmap)
                            {
                                wb = ii as WriteableBitmap;
                            }
                            if (wb != null)
                            {

                                wb.SaveJpeg(isfs, wb.PixelWidth, wb.PixelHeight, 0, 95);
                                long size = 0;
                                if (IsolatedStorageSettings.ApplicationSettings.Contains("bgsize"))
                                {
                                    size = (long)IsolatedStorageSettings.ApplicationSettings["bgsize"];
                                }
                                IsolatedStorageSettings.ApplicationSettings["bgsize"] = size - lastsize + isfs.Length;
                                IsolatedStorageSettings.ApplicationSettings.Save();
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region 가사창 상단
        private void btnLyrics_eye_Click(object sender, RoutedEventArgs e)
        {
            if (ocLyrics.Count > 0)
            {
                fullscreen = LayoutRoot.RowDefinitions[1].Height == GridLength.Auto;
                ToggleFullScreen(fullscreen);
            }
        }

        private void lbLyrics_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            litem = lbLyrics.SelectedIndex;
        }

        private void btnLyrics_refresh_Click(object sender, RoutedEventArgs e)
        {
            lrc = "";
            GetLyrics(true);
        }

        private void btnLyrics_search_Click(object sender, RoutedEventArgs e)
        {
            if (gdSearch.Visibility == System.Windows.Visibility.Visible)
            {
                gdSearch.Visibility = System.Windows.Visibility.Collapsed;
                bdSearch.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                gdSearch.Visibility = System.Windows.Visibility.Visible;
                bdSearch.Visibility = System.Windows.Visibility.Visible;
                ptbTitle.Text = rtitle;
                if (rtitle.Length > 0)
                {
                    ptbTitle.ActualHintVisibility = Visibility.Collapsed;
                }
                ptbArtist.Text = rartist;
                if (rartist.Length > 0)
                {
                    ptbArtist.ActualHintVisibility = Visibility.Collapsed;
                }
                llsLyrics.ItemsSource = ocLR;
                if (ocLR.Count == 0)
                {
                    SearchLyrics(ptbTitle.Text, ptbArtist.Text);
                }
            }
        }

        #endregion

        #region 가사 검색
        ObservableCollection<LyricsRecord> ocLR = new ObservableCollection<LyricsRecord>();
        int lpage = 1;
        private void SearchLyrics(string title, string artist)
        {
            if (MediaPlayer.Queue.ActiveSong != null)
            {
                Song s = MediaPlayer.Queue.ActiveSong;                

                string reqstr = string.Format("/ALSongMobile/MobileSearchLyric.aspx?mode=search&title={0}&artist={1}&album={2}&playtime={3}&page={4}&key={5}",
                HttpUtility.UrlEncode(title), HttpUtility.UrlEncode(artist), HttpUtility.UrlEncode(s.Album.Name), (Convert.ToInt32(s.Duration.TotalSeconds) * 1000),
                lpage, ranstr.Substring(0, Math.Min(256, ranstr.Length)));

                ShowGauge(true, Res.LocalString.searchlyrics);
                ptbTitle.IsEnabled = false;
                ptbArtist.IsEnabled = false;
                Communication.CommunicateALSong(reqstr, (suc, res) =>
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        ShowGauge(false, Res.LocalString.searchlyrics);
                        if (suc >= 0)
                        {
                            llsLyrics.Focus();
                            FillLyricsList(res, lpage == 1);
                        }
                        ptbTitle.IsEnabled = true;
                        ptbArtist.IsEnabled = true;
                    });
                });
            }
        }

        bool isLastpage = false;
        private void FillLyricsList(Stream res, bool clear)
        {
            try
            {
                if (clear)
                {
                    ocLR.Clear();
                }

                List<LyricsRecord> lNew = new List<LyricsRecord>();
                using (Stream s = res)
                {
                    using (XmlReader xr = XmlReader.Create(s))
                    {
                        while (xr.ReadToFollowing("lyric"))
                        {
                            string lyricID = xr["ID"];
                            if (ocLR.IndexOf(new LyricsRecord() { LyricID = lyricID }) >= 0)
                            {
                                continue;
                            }
                            string title = "", artist = "", time = "", desc = "";
                            int up = 0, down = 0;
                            if (xr.ReadToDescendant("title"))
                            {
                                xr.MoveToContent();
                                title = HttpUtility.HtmlDecode(xr.ReadInnerXml());
                            }
                            if (xr.ReadToNextSibling("artist"))
                            {
                                xr.MoveToContent();
                                artist = HttpUtility.HtmlDecode(xr.ReadInnerXml());
                            }
                            if (xr.ReadToNextSibling("album"))
                            {
                                xr.MoveToContent();
                                artist += ", " + HttpUtility.HtmlDecode(xr.ReadInnerXml());
                            }
                            if (xr.ReadToNextSibling("goodCount"))
                            {
                                xr.MoveToContent();
                                string good = xr.ReadInnerXml();
                                try
                                {
                                    up = Convert.ToInt32(good);
                                }
                                catch
                                {
                                    up = 0;
                                }
                                if (desc.Length > 0 && up > 0)
                                {
                                    desc += string.Format(",  {0}: {1}", Res.LocalString.good, up);
                                }
                                else if (up > 0)
                                {
                                    desc += string.Format("{0}: {1}", Res.LocalString.good, up);
                                }
                            }
                            if (xr.ReadToNextSibling("badCount"))
                            {
                                xr.MoveToContent();
                                string bad = xr.ReadInnerXml();
                                try
                                {
                                    down = Convert.ToInt32(bad);
                                }
                                catch
                                {
                                    down = 0;
                                }
                                if (desc.Length > 0 && down > 0)
                                {
                                    desc += string.Format(",  {0}: {1}", Res.LocalString.bad, down);
                                }
                                else if (down > 0)
                                {
                                    desc += string.Format("{0}: {1}", Res.LocalString.bad, down);
                                }
                            }
                            if (xr.ReadToNextSibling("playtime"))
                            {
                                xr.MoveToContent();
                                string timeimsi = xr.ReadInnerXml();
                                int timems = 0;
                                try
                                {
                                    timems = Convert.ToInt32(timeimsi);
                                }
                                catch
                                {
                                    timems = 0;
                                }
                                if (timems > 0)
                                {
                                    int min = Convert.ToInt32(Math.Floor(timems / 1000 / 60.0));
                                    int sec = Convert.ToInt32(Math.Floor((timems - min * 60 * 1000) / 1000.0));
                                    time = string.Format("{0:D2}:{1:D2}", min, sec);
                                }
                            }
                            if (xr.ReadToNextSibling("regDate"))
                            {
                                xr.MoveToContent();
                                if (desc.Length > 0)
                                {
                                    desc += string.Format(",  {0}", xr.ReadInnerXml());
                                }
                                else
                                {
                                    desc += string.Format("{0}", xr.ReadInnerXml());
                                }
                            }
                            lNew.Add(new LyricsRecord(artist,
                                title, desc, time, up, down, lyricID));
                        }
                    }
                }
                if (lNew.Count > 0)
                {
                    if (lNew.Count >= 20)
                    {
                        isLastpage = false;
                    }
                    lpage++;
                }
                else
                {
                    isLastpage = true;
                }
                foreach (LyricsRecord sl in lNew)
                {
                    ocLR.Add(sl);
                }
            }
            catch
            {
                ShowError(Res.LocalString.lyricserrorxml);
            }
        }

        private void ptbTitle_ActionIconTapped(object sender, EventArgs e)
        {
            PhoneTextBox ptb = sender as PhoneTextBox;
            ptb.Text = "";
        }

        private void ptbTitle_GotFocus(object sender, RoutedEventArgs e)
        {
            PhoneTextBox ptb = sender as PhoneTextBox;
            ptb.ActionIcon = new BitmapImage(new Uri("/Images/cancel.png", UriKind.Relative));
        }

        private void ptbTitle_LostFocus(object sender, RoutedEventArgs e)
        {
            PhoneTextBox ptb = sender as PhoneTextBox;
            ptb.ActionIcon = null;
        }

        private void ptbTitle_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                lpage = 1;
                SearchLyrics(ptbTitle.Text, ptbArtist.Text);
                llsLyrics.ItemsSource = ocLR;
            }
        }

        private void llsLyrics_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (llsLyrics.SelectedItem != null)
            {
                GetLyrics(true, (llsLyrics.SelectedItem as LyricsRecord).LyricID);
            }
        }

        DateTime lasttry = DateTime.MinValue;
        private void llsLyrics_Realized(object sender, ItemRealizationEventArgs e)
        {
            if (ocLR.Count >= 20)
            {
                var x = e.Container;
                if (x == null || x.Content == null)
                    return;
                if (lasttry.AddSeconds(5) < DateTime.Now && ocLR.IndexOf(x.Content as LyricsRecord) == ocLR.Count - 1)
                {
                    if (!isLastpage)
                    {
                        lasttry = DateTime.Now;
                        SearchLyrics(ptbTitle.Text, ptbArtist.Text);
                    }
                }
            }
        }  

        #endregion

        #region 메뉴
        private void MenuItemMore_Click(object sender, EventArgs e)
        {
            if (rartist.Length == 0 && rtitle.Length == 0)
            {
                return;
            }
            WebBrowserTask wbTask = new WebBrowserTask();
            wbTask.Uri = new Uri("http://www.google.com/m?q=" + SimplizeText(rartist) + "+" + SimplizeText(rtitle), UriKind.RelativeOrAbsolute);
            wbTask.Show();
        }

        private void MenuItemAd_Click(object sender, EventArgs e)
        {
            MarketplaceDetailTask mdt = new MarketplaceDetailTask();
            mdt.ContentType = MarketplaceContentType.Applications;
            mdt.ContentIdentifier = "df3d33af-1007-462f-8fa8-c30443bdb36b";
            mdt.Show();
        }
        #endregion

        #region 가사 세부
        private void lbLyrics_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (ocLyrics.Count > 0)
            {
                fullscreen = LayoutRoot.RowDefinitions[1].Height == GridLength.Auto;
                ToggleFullScreen(fullscreen);
            }
        }

        bool fullscreen = false, live = true;
        private void ToggleFullScreen(bool visible)
        {
            if (visible)
            {
                SystemTray.IsVisible = false;

                LayoutRoot.RowDefinitions[0].Height = new GridLength(0);
                LayoutRoot.RowDefinitions[1].Height = new GridLength(0);
                LayoutRoot.RowDefinitions[3].Height = new GridLength(0);
                spTool.Visibility = System.Windows.Visibility.Collapsed;
                tbLyrics.Opacity = 0;
                lbLyrics.ItemContainerStyle = this.Resources["FullListStyle"] as Style;

                LyricsPanel.Background = new SolidColorBrush(Colors.Black);

                ApplicationBar.IsVisible = false;

                SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
            }
            else
            {
                SystemTray.IsVisible = true;

                LayoutRoot.RowDefinitions[0].Height = new GridLength(32);
                LayoutRoot.RowDefinitions[1].Height = GridLength.Auto;
                LayoutRoot.RowDefinitions[3].Height = GridLength.Auto;
                spTool.Visibility = System.Windows.Visibility.Visible;
                tbLyrics.Opacity = 1;
                lbLyrics.ItemContainerStyle = this.Resources["lbContainer"] as Style;

                LyricsPanel.Background = new SolidColorBrush(Color.FromArgb(178, 0, 0, 0));

                ApplicationBar.IsVisible = true;

                SupportedOrientations = SupportedPageOrientation.Portrait;
            }
            PhoneApplicationService.Current.State["fs"] = fullscreen;
            lbLyrics.Focus();
        }

        private void pvLyrics_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int iv = -1;
            if (pvLyrics.SelectedIndex == 0)
            {
                if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics0"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics10"] as DataTemplate;
                    ShowInfo(Res.LocalString.view4);
                    iv = 4;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics1"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics0"] as DataTemplate;
                    ShowInfo(Res.LocalString.view0);
                    iv = 0;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics2"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics1"] as DataTemplate;
                    ShowInfo(Res.LocalString.view1);
                    iv = 1;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics5"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics2"] as DataTemplate;
                    ShowInfo(Res.LocalString.view2);
                    iv = 2;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics10"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics5"] as DataTemplate;
                    ShowInfo(Res.LocalString.view3);
                    iv = 3;
                }
            }
            else if (pvLyrics.SelectedIndex == 2)
            {
                if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics0"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics1"] as DataTemplate;
                    ShowInfo(Res.LocalString.view1);
                    iv = 1;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics1"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics2"] as DataTemplate;
                    ShowInfo(Res.LocalString.view2);
                    iv = 2;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics2"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics5"] as DataTemplate;
                    ShowInfo(Res.LocalString.view3);
                    iv = 3;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics5"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics10"] as DataTemplate;
                    ShowInfo(Res.LocalString.view4);
                    iv = 4;
                }
                else if (lbLyrics.ItemTemplate == App.Current.Resources["dtLyrics10"])
                {
                    lbLyrics.ItemTemplate = App.Current.Resources["dtLyrics0"] as DataTemplate;
                    ShowInfo(Res.LocalString.view0);
                    iv = 0;
                }
            }
            if (iv >= 0)
            {
                IsolatedStorageSettings.ApplicationSettings["view"] = iv;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
            lbLyrics.ItemsSource = null;
            lbLyrics.ItemsSource = ocLyrics;

            if (pvLyrics.SelectedIndex != 1)
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    pvLyrics.SelectionChanged -= pvLyrics_SelectionChanged;
                    pvLyrics.Visibility = System.Windows.Visibility.Collapsed;
                    if (pvLyrics.SelectedIndex == 0)
                    {
                        pvLyrics.SelectedIndex = 2;
                        pvLyrics.ApplyTemplate();
                        pvLyrics.SelectedIndex = 1;
                        pvLyrics.ApplyTemplate();
                    }
                    else
                    {
                        pvLyrics.SelectedIndex = 0;
                        pvLyrics.ApplyTemplate();
                        pvLyrics.SelectedIndex = 1;
                        pvLyrics.ApplyTemplate();
                    }
                    pvLyrics.Visibility = System.Windows.Visibility.Visible;
                    pvLyrics.SelectionChanged += pvLyrics_SelectionChanged;
                });
            }
        }
        #endregion

        PageOrientation poLast = PageOrientation.PortraitUp;
        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            int i = 0, j = 0, k = 0;
            switch(poLast)
            {
                case PageOrientation.PortraitUp:
                    i = 0; break;
                case PageOrientation.LandscapeLeft:
                    i = 1; break;
                case PageOrientation.LandscapeRight:
                    i = -1; break;
            }
            switch (e.Orientation)
            {
                case PageOrientation.PortraitUp:
                    j = 0; break;
                case PageOrientation.LandscapeLeft:
                    j = 1; break;
                case PageOrientation.LandscapeRight:
                    j = -1; break;
            }

            k = i - j;
            if(Math.Abs(k) == 1)
            {
                if(k < 0)
                {
                    StartTransition(RotateTransitionMode.In90Clockwise);
                }
                else
                {
                    StartTransition(RotateTransitionMode.In90Counterclockwise);
                }
            }
            else if(Math.Abs(k) == 2)
            {
                if (k < 0)
                {
                    StartTransition(RotateTransitionMode.In180Clockwise);
                }
                else
                {
                    StartTransition(RotateTransitionMode.In180Counterclockwise);
                }
            }
            poLast = e.Orientation;
        }

        private void bdThumb_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            btnLyrics_Click(btnLyrics, null);
        }

        private void btnSub_Enter(object sender, MouseEventArgs e)
        {
            ShowInfo((sender as Button).Tag as string);
        }

        private void btnSub_Leave(object sender, MouseEventArgs e)
        {
            HideInfo();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

       

        

    }
}